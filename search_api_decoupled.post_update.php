<?php

/**
 * @file
 * Post update hooks for search_api_decoupled module.
 */

use Drupal\Core\Config\Entity\ConfigEntityUpdater;
use Drupal\search_api_decoupled\SearchApiEndpointInterface;

/**
 * Set defaults for "skip_field_extraction" and "ensure_result_item_url".
 */
function search_api_decoupled_post_update_10001(&$sandbox) {
  $config_entity_updater = \Drupal::classResolver(ConfigEntityUpdater::class);
  $callback = function (SearchApiEndpointInterface $search_api_endpoint) {
    $search_api_endpoint->set('skip_field_extraction', FALSE);
    $search_api_endpoint->set('ensure_result_item_url', TRUE);
    return TRUE;
  };
  $config_entity_updater->update($sandbox, 'search_api_endpoint', $callback);
}
