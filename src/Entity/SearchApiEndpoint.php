<?php

namespace Drupal\search_api_decoupled\Entity;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Url;
use Drupal\search_api_decoupled\Event\SearchApiEndpointFacets;
use Drupal\search_api_decoupled\SearchApiEndpointInterface;

/**
 * Defines the Search API Endpoint entity.
 *
 * @ConfigEntityType(
 *   id = "search_api_endpoint",
 *   label = @Translation("Search API Endpoint"),
 *   handlers = {
 *     "list_builder" = "Drupal\search_api_decoupled\SearchApiEndpointListBuilder",
 *     "form" = {
 *       "add" = "Drupal\search_api_decoupled\Form\SearchApiEndpointForm",
 *       "edit" = "Drupal\search_api_decoupled\Form\SearchApiEndpointForm",
 *       "delete" = "Drupal\search_api_decoupled\Form\SearchApiEndpointDeleteForm"
 *     }
 *   },
 *   config_prefix = "search_api_endpoint",
 *   admin_permission = "administer search_api_enpoint",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/search/search-api/endpoints/{search_api_endpoint}/edit",
 *     "delete-form" = "/admin/config/search/search-api/endpoints/{search_api_endpoint}/delete",
 *     "collection" = "/admin/config/search/search-api/endpoints"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "index",
 *     "limit",
 *     "items_per_page_options",
 *     "searched_fields",
 *     "excluded_fields",
 *     "parse_mode",
 *     "default_sort",
 *     "default_sort_order",
 *     "skip_field_extraction",
 *     "ensure_result_item_url"
 *   }
 * )
 */
class SearchApiEndpoint extends ConfigEntityBase implements SearchApiEndpointInterface {

  /**
   * The Search endpoint ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Search endpoint label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Search endpoint description.
   *
   * @var string
   */
  protected $description;

  /**
   * The Search Api index.
   *
   * @var string
   */
  protected $index;

  /**
   * The limit per page.
   *
   * @var string
   */
  protected $limit = 10;

  /**
   * The items per page allowed.
   *
   * @var array
   */
  protected $items_per_page_options;

  /**
   * The searched fields.
   *
   * @var array
   */
  protected $searched_fields = [];

  /**
   * The excluded fields.
   *
   * @var array
   */
  protected $excluded_fields = [];

  /**
   * The query parse mode.
   *
   * @var string
   */
  protected $parse_mode = 'direct';

  /**
   * Flag tracking if extracting extra fields is skipped.
   *
   * @var bool
   */
  protected $skip_field_extraction = FALSE;

  /**
   * Flag tracking if building a missing URL skipped.
   *
   * @var bool
   */
  protected $ensure_result_item_url = TRUE;

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    parent::calculateDependencies();
    $index = \Drupal::entityTypeManager()->getStorage('search_api_index')->load($this->getIndex());
    $this->addDependency('config', $index->getConfigDependencyName());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getIndex() {
    return $this->index;
  }

  /**
   * {@inheritdoc}
   */
  public function getLimit() {
    return $this->limit;
  }

  /**
   * {@inheritdoc}
   */
  public function getItemsPerPage() {
    return $this->items_per_page_options;
  }

  /**
   * {@inheritdoc}
   */
  public function isAllowedItemsPerPage($limit) {
    $items_per_page = $this->getItemsPerPage() ?? [];
    return in_array($limit, $items_per_page);
  }

  /**
   * {@inheritdoc}
   */
  public function getSearchedFields() {
    return $this->searched_fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getExcludedFields() {
    return $this->excluded_fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getFulltextFields() {
    if (empty($this->index)) {
      return [];
    }

    /** @var  \Drupal\search_api\IndexInterface $index */
    $index = \Drupal::entityTypeManager()->getStorage('search_api_index')->load($this->index);

    $fields = [];
    $fields_info = $index->getFields();
    foreach ($index->getFulltextFields() as $field_id) {
      $fields[$field_id] = $fields_info[$field_id]->getPrefixedLabel();
    }

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIndexedFields() {
    if (empty($this->index)) {
      return [];
    }

    /** @var  \Drupal\search_api\IndexInterface $index */
    $index = \Drupal::entityTypeManager()->getStorage('search_api_index')->load($this->index);

    $fields = [];
    $fields_info = $index->getFields();
    foreach ($fields_info as $field_id => $field_info) {
      $fields[$field_id] = $field_info->getPrefixedLabel();
    }

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIndexFieldsDefinitions() {
    if (empty($this->index)) {
      return [];
    }

    /** @var  \Drupal\search_api\IndexInterface $index */
    $index = \Drupal::entityTypeManager()->getStorage('search_api_index')->load($this->index);

    return $index->getFields();
  }

  /**
   * {@inheritdoc}
   */
  public function getParseMode() {
    return $this->parse_mode;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function getFacets() {
    $facets = [];
    $event = new SearchApiEndpointFacets($this, $facets);
    \Drupal::service('event_dispatcher')->dispatch($event, 'search_api_decoupled.search_endpoint_facets_alter');
    return $event->getFacets();
  }

  /**
   * {@inheritdoc}
   */
  public function getAutocomplete() {
    if (\Drupal::moduleHandler()->moduleExists('search_api_decoupled_autocomplete') && !$this->isNew()) {
      return \Drupal::entityTypeManager()->getStorage('search_api_autocomplete_search')->load($this->id());
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseUrl() {
    return Url::fromRoute('search_api_decoupled.search_results', ['search_api_endpoint' => $this->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    $tags = parent::getCacheTags();
    // Add cache tags of all the facets.
    foreach ($this->getFacets() as $facet) {
      $tags = Cache::mergeTags($tags, $facet->getCacheTags());
    }
    // Add cache tags of the autocomplete.
    $autocomplete = $this->getAutocomplete();
    if ($autocomplete) {
      $tags = Cache::mergeTags($tags, $autocomplete->getCacheTags());
    }
    // Finally add search api index cache tag.
    return Cache::mergeTags($tags, ['search_api_index:' . $this->getIndex()]);
  }

  /**
   * Get available exposed filter operators.
   *
   * @return array
   *   The list of available operators.
   */
  public static function getOperators() {
    return [
      'equal' => t('Equal'),
      'between' => t('Between'),
      'gt' => t('Greater than'),
      'gte' => t('Greater than or equal'),
      'lt' => t('Less than'),
      'lte' => t('Less than or equal'),
      'not_equal' => t('Not equal'),
      'not_in' => t('Not in'),
      'in' => t('In'),
    ];
  }

  /**
   * Get the condition operator.
   *
   * @param string $operator
   *   The operator.
   *
   * @return string
   *   The condition operator.
   */
  public static function getConditionOperator($operator) {
    $mapping = [
      'equal' => '=',
      'between' => 'BETWEEN',
      'gt' => '>',
      'gte' => '>=',
      'lt' => '<',
      'lte' => '<=',
      'not_equal' => '<>',
      'not_in' => 'NOT IN',
      'in' => 'IN',
    ];
    return $mapping[$operator] ?? '=';
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultSort() {
    return $this->default_sort ?? 'search_api_relevance';
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultSortOrder() {
    return $this->default_sort_order ?? 'desc';
  }

  /**
   * {@inheritdoc}
   */
  public function getSortAllowedFields() {
    $fields = [];
    foreach ($this->getIndexFieldsDefinitions() as $field => $field_definition) {
      // Make sure that text fields are not listed for sorting.
      if (!\Drupal::getContainer()
        ->get('search_api.data_type_helper')
        ->isTextType($field_definition->getType())) {
        $fields[$field] = $field_definition->getLabel();
      }
    }
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function skipFieldExtraction() {
    return $this->skip_field_extraction;
  }

  /**
   * {@inheritdoc}
   */
  public function ensureResultItemUtl() {
    return $this->ensure_result_item_url;
  }

}
