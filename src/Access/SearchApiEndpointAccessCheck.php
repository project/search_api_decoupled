<?php

namespace Drupal\search_api_decoupled\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultReasonInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\search_api_decoupled\SearchApiEndpointInterface;

/**
 * Checks access for displaying search_api_endpoint results.
 */
class SearchApiEndpointAccessCheck implements AccessInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new SearchApiEndpointAccessCheck object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function access(SearchApiEndpointInterface $search_api_endpoint, AccountInterface $account) {
    $permission = 'use search with ' . $search_api_endpoint->id() . ' endpoint';
    /** @var \Drupal\search_api\IndexInterface $index */
    $index = $this->entityTypeManager->getStorage('search_api_index')->load($search_api_endpoint->getIndex());
    $access = AccessResult::allowedIf($index->status())
      ->andIf(AccessResult::allowedIfHasPermissions(
        $account, [$permission, 'administer search_api_endpoint'], 'OR'))
      ->addCacheableDependency($index)
      ->addCacheableDependency($search_api_endpoint);
    if ($access instanceof AccessResultReasonInterface) {
      $access->setReason("The \"$permission\" permission is required and search index for this search must be enabled.");
    }
    return $access;
  }

}
