<?php

namespace Drupal\search_api_decoupled\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\search_api_decoupled\SearchApiEndpointInterface;

/**
 * Class containing the configuration of SearchApiEndpointFacets.
 *
 * @package Drupal\search_api_decoupled\Event
 */
class SearchApiEndpointFacets extends Event {

  /**
   * The search endpoint.
   *
   * @var \Drupal\search_api_decoupled\SearchApiEndpointInterface
   */
  protected $searchApiEndpoint;

  /**
   * The facets.
   *
   * @var \Drupal\facets\FacetInterface[]
   */
  protected $facets;

  /**
   * SearchApiEndpointFacets constructor.
   *
   * @param \Drupal\search_api_decoupled\SearchApiEndpointInterface $search_api_endpoint
   *   The search config.
   * @param \Drupal\facets\FacetInterface[] $facets
   *   The facets.
   */
  public function __construct(SearchApiEndpointInterface $search_api_endpoint, array $facets) {
    $this->searchApiEndpoint = $search_api_endpoint;
    $this->facets = $facets;
  }

  /**
   * Return search api endpoint.
   *
   * @return \Drupal\search_api_decoupled\SearchApiEndpointInterface
   *   The search api endpoint.
   */
  public function getSearchApiEndpoint() {
    return $this->searchApiEndpoint;
  }

  /**
   * Return the the facets.
   *
   * @return \Drupal\facets\FacetInterface[]
   *   The facets.
   */
  public function getFacets() {
    return $this->facets;
  }

  /**
   * Set the facets.
   *
   * @param \Drupal\facets\FacetInterface[] $facets
   *   The facets.
   */
  public function setFacets(array $facets) {
    $this->facets = $facets;
  }

}
