<?php

namespace Drupal\search_api_decoupled\Event;

/**
 * Defines events for the search_api_decoupled module.
 */
final class SearchApiEndpointEvents {

  /**
   * Name of the event fired when altering search results.
   *
   * @Event
   *
   * @see \Drupal\search_api_decoupled\Event\SearchApiEndpointResultsAlter
   */
  const SEARCH_RESULTS_ALTER = 'search_api_decoupled.search_results_alter';

  /**
   * Name of the event fired when altering search endpoint facets.
   *
   * @Event
   *
   * @see \Drupal\search_api_decoupled\Event\SearchApiDecoupledFacetsAlter
   */
  const SEARCH_CONFIG_FACETS_ALTER = 'search_api_decoupled.search_endpoint_facets_alter';

}
