<?php

namespace Drupal\search_api_decoupled\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\search_api\Query\ResultSetInterface;
use Drupal\search_api_decoupled\SearchApiEndpointInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Alter search api endpoint results.
 *
 * @package Drupal\search_api_decoupled\Event
 */
class SearchApiEndpointResultsAlter extends Event {

  /**
   * The search api endpoint.
   *
   * @var \Drupal\search_api_decoupled\SearchApiEndpointInterface
   */
  protected $searchApiEndpoint;

  /**
   * The search api result.
   *
   * @var \Drupal\search_api\Query\ResultSetInterface
   */
  protected $result;

  /**
   * The search api response.
   *
   * @var array
   */
  protected $response;

  /**
   * The search api cacheable metadata.
   *
   * @var \Drupal\Core\Cache\CacheableMetadata
   */
  protected $cacheableMetadata;

  /**
   * The request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected Request $request;

  /**
   * Constructs a new SearchApiEndpointResultsAlter object.
   *
   * @param \Drupal\search_api_decoupled\SearchApiEndpointInterface $search_api_endpoint
   *   The search api config.
   * @param \Drupal\search_api\Query\ResultSetInterface $result
   *   The search api result.
   * @param array $response
   *   The search api response.
   * @param \Drupal\Core\Cache\CacheableMetadata|null $cacheable_metadata
   *   The cacheable metadata of response (optional).
   * @param \Symfony\Component\HttpFoundation\Request|null $request
   *   The request object (optional).
   */
  public function __construct(SearchApiEndpointInterface $search_api_endpoint, ResultSetInterface $result, array $response, CacheableMetadata $cacheable_metadata = NULL, Request $request = NULL) {
    $this->searchApiEndpoint = $search_api_endpoint;
    $this->result = $result;
    $this->response = $response;
    if (is_null($cacheable_metadata)) {
      $cacheable_metadata = new CacheableMetadata();
    }
    $this->cacheableMetadata = $cacheable_metadata;
    $this->request = $request;
  }

  /**
   * Return the search api endpoint.
   *
   * @return \Drupal\search_api_decoupled\SearchApiEndpointInterface
   *   The search api endpoint.
   */
  public function getSearchApiEndpoint() {
    return $this->searchApiEndpoint;
  }

  /**
   * Return the result.
   *
   * @return \Drupal\search_api\Query\ResultSetInterface
   *   The search result.
   */
  public function getResult() {
    return $this->result;
  }

  /**
   * Return the response.
   *
   * @return array
   *   The response.
   */
  public function getResponse() {
    return $this->response;
  }

  /**
   * Return the request.
   *
   * @return \Symfony\Component\HttpFoundation\Request
   *   The request object.
   */
  public function getRequest() {
    return $this->request;
  }

  /**
   * Set the response.
   *
   * @param array $response
   *   The response.
   */
  public function setResponse(array $response) {
    $this->response = $response;
  }

  /**
   * Set chacheable metadata.
   *
   * @param \Drupal\Core\Cache\CacheableMetadata $cacheable_metadata
   *   The metadata object.
   */
  public function setCacheableMetadata(CacheableMetadata $cacheable_metadata) {
    $this->cacheableMetadata = $cacheable_metadata;
  }

  /**
   * Return the cacheable metadata.
   *
   * @return \Drupal\Core\Cache\CacheableMetadata
   *   The cacheable metadata object.
   */
  public function getCacheableMetadata() {
    return $this->cacheableMetadata;
  }

}
