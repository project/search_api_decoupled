<?php

namespace Drupal\search_api_decoupled;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides permissions of the search_api_decoupled module.
 */
class SearchApiEndpointPermissions implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a Permissions object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Returns a list of permissions, one per search endpoint.
   *
   * @return array[]
   *   A list of permission definitions, keyed by permission machine name.
   */
  public function byEndpoint() {
    $perms = [];
    /** @var \Drupal\search_api_decoupled\SearchApiEndpointInterface $search */
    foreach ($this->entityTypeManager->getStorage('search_api_endpoint')->loadMultiple() as $id => $search) {
      $perms['use search with ' . $id . ' endpoint'] = [
        'title' => $this->t('Use search with %search endpoint', ['%search' => $search->label()]),
      ];
    }
    return $perms;
  }

}
