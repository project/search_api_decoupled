<?php

namespace Drupal\search_api_decoupled\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SearchApiEndpointForm contains form for add/edit of endpoints.
 *
 * @package Drupal\search_api_decoupled\Form
 */
class SearchApiEndpointForm extends EntityForm {

  /**
   * The parse mode plugin manager.
   *
   * @var \Drupal\search_api\ParseMode\ParseModePluginManager
   */
  protected $parseModeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->parseModeManager = $container->get('plugin.manager.search_api.parse_mode');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\search_api_decoupled\SearchApiEndpointInterface $search_api_endpoint */
    $search_api_endpoint = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#maxlength' => 255,
      '#default_value' => $search_api_endpoint->label(),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $search_api_endpoint->id(),
      '#machine_name' => [
        'exists' => '\Drupal\search_api_decoupled\Entity\SearchApiEndpoint::load',
      ],
      '#disabled' => !$search_api_endpoint->isNew(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#rows' => 5,
      '#default_value' => $search_api_endpoint->getDescription(),
    ];

    // Default index and states.
    $default_index = $search_api_endpoint->getIndex();
    $default_index_states = [
      'visible' => [
        ':input[name="index"]' => ['value' => $default_index],
      ],
    ];

    $index_options = [];
    $search_api_indexes = $this->entityTypeManager->getStorage('search_api_index')->loadMultiple();
    /** @var  \Drupal\search_api\IndexInterface $search_api_index */
    foreach ($search_api_indexes as $search_api_index) {
      $index_options[$search_api_index->id()] = $search_api_index->label();
    }

    $form['index_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Index'),
      '#tree' => FALSE,
    ];

    $form['index_fieldset']['index'] = [
      '#type' => 'select',
      '#title' => $this->t('Search API index'),
      '#options' => $index_options,
      '#default_value' => $default_index,
      '#required' => TRUE,
    ];

    $form['index_fieldset']['previous_index'] = [
      '#type' => 'value',
      '#value' => $default_index,
    ];

    $searched_fields = $search_api_endpoint->getFullTextFields();
    $form['index_fieldset']['searched_fields'] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#options' => $searched_fields,
      '#size' => min(4, count($searched_fields)),
      '#title' => $this->t('Searched fields'),
      '#description' => $this->t('Select the fields that will be searched. If no fields are selected, all available fulltext fields will be searched.'),
      '#default_value' => $search_api_endpoint->getSearchedFields(),
      '#access' => !empty($default_index),
      '#states' => $default_index_states,
    ];

    $indexed_fields = $search_api_endpoint->getIndexedFields();
    $form['index_fieldset']['excluded_fields'] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#options' => $indexed_fields,
      '#size' => min(4, count($indexed_fields)),
      '#title' => $this->t('Excluded fields'),
      '#description' => $this->t('Select the fields that should not be returned by the search.'),
      '#default_value' => $search_api_endpoint->getExcludedFields(),
      '#access' => !empty($default_index),
      '#states' => $default_index_states,
    ];

    $form['page_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Page'),
      '#states' => [
        'visible' => [':input[name="index"]' => ['value' => $default_index]],
      ],
      '#access' => !empty($default_index),
      '#tree' => FALSE,
    ];

    $form['page_fieldset']['limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Limit'),
      '#default_value' => $search_api_endpoint->getLimit(),
      '#min' => 1,
      '#required' => TRUE,
      '#access' => !empty($default_index),
      '#states' => $default_index_states,
    ];

    $form['page_fieldset']['items_per_page_options'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Items per page options'),
      '#description' => $this->t('Items per page allowed to be send in the query parameter "limit". The allowed options are exposed in the uiSettings. Enter a list of numbers separated by comma. Example: 10,20,50,100'),
      '#default_value' => $search_api_endpoint->getItemsPerPage() ? implode(',', $search_api_endpoint->getItemsPerPage()) : '',
      '#access' => !empty($default_index),
      '#states' => $default_index_states,
    ];

    $sort_fields = [
      '' => $this->t('None'),
      'search_api_relevance' => $this->t('Relevance'),
    ] + $search_api_endpoint->getSortAllowedFields();
    $form['page_fieldset']['default_sort'] = [
      '#type' => 'select',
      '#title' => $this->t('Default sort field'),
      '#default_value' => $search_api_endpoint->getDefaultSort(),
      '#options' => $sort_fields,
      '#access' => !empty($default_index),
      '#states' => $default_index_states,
    ];

    $form['page_fieldset']['default_sort_order'] = [
      '#type' => 'select',
      '#title' => $this->t('Default sort order'),
      '#default_value' => $search_api_endpoint->getDefaultSortOrder(),
      '#options' => [
        'asc' => $this->t('Ascending'),
        'desc' => $this->t('Descending'),
      ],
      '#access' => !empty($default_index),
      '#states' => $default_index_states,
    ];

    $instances = $this->parseModeManager->getInstances();
    $options = [];
    foreach ($instances as $name => $instance) {
      if ($instance->isHidden()) {
        continue;
      }
      $options[$name] = $instance->label();
    }

    $form['page_fieldset']['parse_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Parse mode'),
      '#description' => $this->t('Parse mode for search keywords'),
      '#options' => $options,
      '#default_value' => $search_api_endpoint->getParseMode(),
      '#required' => TRUE,
    ];

    $form['skip_field_extraction'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Skip extracting (extra) fields from original objects.'),
      '#description' => $this->t('Check if you want to avoid additional entities being loaded. Only fields present in the search result will be present.'),
      '#default_value' => $search_api_endpoint->skipFieldExtraction(),
    ];

    $form['ensure_result_item_url'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Ensure that the result item contains a url to the original object.'),
      '#description' => $this->t('Uncheck if you want to avoid a URL being constructed for every search result, if it is not present in the search result.'),
      '#default_value' => $search_api_endpoint->ensureResultItemUtl(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);

    /** @var \Drupal\search_api_decoupled\SearchApiEndpointInterface $search_api_endpoint */
    $search_api_endpoint = $this->entity;
    if ($search_api_endpoint->isNew()) {
      $actions['submit']['#value'] = $this->t('Next');
    }

    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $items_per_page_options = $form_state->getValue('items_per_page_options');
    if (!empty($items_per_page_options)) {
      $options = explode(',', $items_per_page_options);
      // Convert all options to integers.
      $intOptions = array_map('intval', $options);

      // Check if any option is not a positive integer.
      if (in_array(FALSE, $intOptions, TRUE) || in_array(0, $intOptions, TRUE) || min($intOptions) < 1) {
        $form_state->setErrorByName('items_per_page_options', $this->t('Items per page options must be a comma-separated list of positive integer numbers.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Make sure limit gets saved as integer for config.
    if (!empty($form_state->getValue('limit'))) {
      $form_state->setValue('limit', (int) $form_state->getValue('limit'));
    }

    // Save the items per page options as array.
    $items_per_page_options = $form_state->getValue('items_per_page_options');
    if (!empty($items_per_page_options)) {
      $items_per_page_options = explode(',', $items_per_page_options);
      $items_per_page_options = array_map('trim', $items_per_page_options);
      $items_per_page_options = array_map('intval', $items_per_page_options);
      $form_state->setValue('items_per_page_options', $items_per_page_options);
    }
    else {
      $form_state->setValue('items_per_page_options', []);
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\search_api_decoupled\SearchApiEndpointInterface $search_api_endpoint */
    $search_api_endpoint = $this->entity;

    // Check searched fields. In case nothing has been selected, select all
    // the available fields.
    $has_selection = FALSE;
    $searched_fields = $form_state->getValue('searched_fields');
    foreach ($searched_fields as $key => $value) {
      if ($key === $value) {
        $has_selection = TRUE;
        break;
      }
    }
    if (!$has_selection) {
      $key_values = array_keys($form['index_fieldset']['searched_fields']['#options']);
      $searched_fields = array_combine($key_values, $key_values);
      $search_api_endpoint->set('searched_fields', $searched_fields);
    }

    $excluded_fields = $form_state->getValue('excluded_fields');
    $excluded_fields = array_combine($excluded_fields, $excluded_fields);
    $search_api_endpoint->set('excluded_fields', $excluded_fields);

    $search_api_endpoint->set('skip_field_extraction', (bool) $form_state->getValue('skip_field_extraction'));
    $search_api_endpoint->set('ensure_result_item_url', (bool) $form_state->getValue('ensure_result_item_url'));

    $status = $search_api_endpoint->save();

    switch ($status) {
      case SAVED_NEW:
        // Redirect to edit form so the rest can be configured.
        $form_state->setRedirectUrl($search_api_endpoint->toUrl('edit-form'));
        break;

      default:
        $indexHasChanged = $form_state->getValue('index') !== $form_state->getValue('previous_index');
        if (!$indexHasChanged) {
          // Index is unchanged so we'll redirect to the overview.
          $this->messenger()->addMessage($this->t('Saved the %label Search page.', [
            '%label' => $search_api_endpoint->label(),
          ]));
        }
        else {
          // Index has changed so we'll redirect to the edit form.
          $this->messenger()->addMessage($this->t('Updated the index for the %label Search page.', [
            '%label' => $search_api_endpoint->label(),
          ]));
        }
    }

  }

}
