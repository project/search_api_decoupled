<?php

namespace Drupal\search_api_decoupled;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Search API Endpoint entities.
 */
interface SearchApiEndpointInterface extends ConfigEntityInterface {

  /**
   * Return the search api index.
   *
   * @return string
   *   The index.
   */
  public function getIndex();

  /**
   * Return the limit per page.
   *
   * @return int
   *   The page limit.
   */
  public function getLimit();

  /**
   * Return the items per page allowed.
   *
   * @return array
   *   The items per page.
   */
  public function getItemsPerPage();

  /**
   * Return if the limit is in the allowed items per page.
   *
   * @param int $limit
   *   The limit to check.
   *
   * @return bool
   *   The limit is allowed.
   */
  public function isAllowedItemsPerPage($limit);

  /**
   * Return the searched fields.
   *
   * @return string[]
   *   A collection of searched fields.
   */
  public function getSearchedFields();

  /**
   * Return the fields that should not be returned by the search.
   *
   * @return string[]
   *   A collection of excluded fields.
   */
  public function getExcludedFields();

  /**
   * Return all indexed fields.
   *
   * @return string[]
   *   A collection of indexed fields.
   */
  public function getIndexedFields();

  /**
   * Retrieves a list of all available fulltext fields.
   *
   * @return string[]
   *   An options list of fulltext field identifiers mapped to their prefixed
   *   labels.
   */
  public function getFullTextFields();

  /**
   * The parse mode to use for query keywords.
   *
   * @return string
   *   Can be any ID of a parse mode plugin.
   */
  public function getParseMode();

  /**
   * Return the description.
   *
   * @return string
   *   The description.
   */
  public function getDescription();

  /**
   * Get the list of facets enabled for this search config.
   *
   * @return \Drupal\facets\FacetInterface[]
   *   The facets.
   */
  public function getFacets();

  /**
   * Gets the base url for given search config.
   *
   * @return \Drupal\Core\Url
   *   The url object.
   */
  public function getBaseUrl();

  /**
   * The autocomplete config for given search config.
   *
   * @return \Drupal\search_api_autocomplete\SearchInterface|null
   *   Autocomplete object or NULL if not set.
   */
  public function getAutocomplete();

  /**
   * Returns a list of all indexed fields of this index.
   *
   * @return \Drupal\search_api\Item\FieldInterface[]
   *   An array of all indexed fields for this index, keyed by field identifier.
   */
  public function getIndexFieldsDefinitions();

  /**
   * Get the default sort field.
   *
   * @return string
   *   The field name.
   */
  public function getDefaultSort();

  /**
   * Get the default sort order.
   *
   * @return string
   *   The sort order: 'asc' or 'desc'.
   */
  public function getDefaultSortOrder();

  /**
   * Get the allowed sort fields.
   *
   * @return array
   *   The allowed sort fields.
   */
  public function getSortAllowedFields();

  /**
   * Check if server data can/should be complemented by original object fields.
   *
   * @return bool
   *   True if server data can/should be complemented.
   */
  public function skipFieldExtraction();

  /**
   * Check if URL to the item should be built if not provided in search result.
   *
   * @return bool
   *   True if URL should be built if none provided by search result.
   */
  public function ensureResultItemUtl();

}
