<?php

namespace Drupal\search_api_decoupled\Plugin\search_api\display;

use Drupal\search_api\Display\DisplayPluginBase;

/**
 * Represents a Search API Endpoint search display.
 *
 * @SearchApiDisplay(
 *   id = "search_api_endpoint",
 *   deriver = "Drupal\search_api_decoupled\Plugin\search_api\display\SearchApiEndpointDeriver"
 * )
 */
class SearchApiEndpoint extends DisplayPluginBase {

  /**
   * {@inheritdoc}
   */
  public function isRenderedInCurrentRequest() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = parent::calculateDependencies();

    $config = $this->getSearchApiEndpoint();
    $dependencies[$config->getConfigDependencyKey()][] = $config->getConfigDependencyName();

    return $dependencies;
  }

  /**
   * Retrieves the endpoint this search display is based on.
   *
   * @returns \Drupal\search_api_decoupled\SearchApiEndpointInterface
   */
  protected function getSearchApiEndpoint() {
    $plugin_definition = $this->getPluginDefinition();
    return $this->getEntityTypeManager()
      ->getStorage('search_api_endpoint')
      ->load($plugin_definition['search_api_endpoint']);
  }

}
