<?php

namespace Drupal\search_api_decoupled\Plugin\search_api\display;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\search_api\Display\DisplayDeriverBase;

/**
 * Derives a display plugin definition for all supported search api endpoints.
 */
class SearchApiEndpointDeriver extends DisplayDeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    if (!isset($this->derivatives)) {
      $this->derivatives = [];

      try {
        /** @var \Drupal\Core\Entity\EntityStorageInterface $search_api_endpoint_storage */
        $search_api_endpoint_storage = $this->entityTypeManager->getStorage('search_api_endpoint');
        $all_endpoints = $search_api_endpoint_storage->loadMultiple();
      }
      catch (PluginNotFoundException $e) {
        return $this->derivatives;
      }

      /** @var \Drupal\search_api_decoupled\SearchApiEndpointInterface $search_api_endpoint */
      foreach ($all_endpoints as $search_api_endpoint) {
        $this->derivatives[$search_api_endpoint->id()] = [
          'label' => $search_api_endpoint->label(),
          'description' => $search_api_endpoint->getDescription(),
          'index' => $search_api_endpoint->getIndex(),
          'search_api_endpoint' => $search_api_endpoint->id(),
        ] + $base_plugin_definition;
      }
    }

    return $this->derivatives;
  }

}
