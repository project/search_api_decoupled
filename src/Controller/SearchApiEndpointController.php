<?php

namespace Drupal\search_api_decoupled\Controller;

use Drupal\Component\Utility\Timer;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Pager\PagerManagerInterface;
use Drupal\Core\Url;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\ParseMode\ParseModePluginManager;
use Drupal\search_api\Utility\Utility;
use Drupal\search_api_decoupled\Entity\SearchApiEndpoint;
use Drupal\search_api_decoupled\Event\SearchApiEndpointEvents;
use Drupal\search_api_decoupled\Event\SearchApiEndpointResultsAlter;
use Drupal\search_api_decoupled\SearchApiEndpointInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller class for search api endpoint.
 *
 * @package Drupal\search_api_decoupled\Controller
 */
class SearchApiEndpointController extends ControllerBase {

  /**
   * The parse mode plugin manager.
   *
   * @var \Drupal\search_api\ParseMode\ParseModePluginManager
   */
  protected $parseModePluginManager;

  /**
   * The pager manager.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected $pagerManager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * SearchApiPageController constructor.
   *
   * @param \Drupal\search_api\ParseMode\ParseModePluginManager $parse_mode_plugin_manager
   *   The parse mode plugin manager.
   * @param \Drupal\Core\Pager\PagerManagerInterface $pager_manager
   *   The pager manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(ParseModePluginManager $parse_mode_plugin_manager, PagerManagerInterface $pager_manager, EventDispatcherInterface $event_dispatcher) {
    $this->parseModePluginManager = $parse_mode_plugin_manager;
    $this->pagerManager = $pager_manager;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.search_api.parse_mode'),
      $container->get('pager.manager'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * Performs a search.
   *
   * @param \Drupal\search_api_decoupled\SearchApiEndpointInterface $search_api_endpoint
   *   The search api endpoint.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Drupal\Core\Cache\CacheableJsonResponse|\Symfony\Component\HttpFoundation\JsonResponse
   *   The response with search results.
   */
  public function searchResults(SearchApiEndpointInterface $search_api_endpoint, Request $request) {
    $query = $this->prepareQuery($request, $search_api_endpoint);
    $keys = $request->get('q', '');
    if (!empty($keys)) {
      $query->keys($keys);
    }
    $search_response = [
      'search_results' => [],
      'search_results_per_page' => 0,
      'search_results_count' => 0,
      'search_results_page' => 0,
      'search_results_pages' => 0,
      'facets' => [],
    ];
    $cacheable_metadata = new CacheableMetadata();
    try {
      Timer::start('search_execution');
      $result = $query->execute();
      $timer = Timer::stop('search_execution');
      $search_response['took'] = $timer['time'];
      $max_score = 0;
      /** @var \Drupal\search_api\Item\ItemInterface[] $items */
      $items = $result->getResultItems();
      foreach ($items as $item) {
        $extracted_fields = [];
        // By default, ItemInterface::getFields tries to load the original
        // object for field extraction.
        $fields = $item->getFields(!$search_api_endpoint->skipFieldExtraction());
        $fields = array_diff_key($fields, $search_api_endpoint->getExcludedFields());
        foreach ($fields as $field) {
          $values = [];
          foreach ($field->getValues() as $value) {
            $values[] = (string) $value;
          }
          if (count($values) == 1) {
            $values = reset($values);
          }
          $extracted_fields[$field->getFieldIdentifier()] = $values;
        }
        $extracted_fields['id'] = $item->getId();
        if (empty($extracted_fields['url']) && $search_api_endpoint->ensureResultItemUtl()) {
          $extracted_fields['url'] = $this->getSearchResultUrl($item, $cacheable_metadata);
        }
        $extracted_fields['score'] = $item->getScore();
        if ($extracted_fields['score'] > $max_score) {
          $max_score = $extracted_fields['score'];
        }
        $extracted_fields['excerpt'] = $item->getExcerpt();
        $search_response['search_results'][] = $extracted_fields;
      }
      $search_response['max_score'] = $max_score;
      $limit = $search_api_endpoint->getLimit();
      $items_per_page = $request->get('limit');
      if (!empty($items_per_page) && $search_api_endpoint->isAllowedItemsPerPage($items_per_page)) {
        $limit = $items_per_page;
      }
      $pager = $this->pagerManager->createPager($result->getResultCount(), $limit);
      // Casting is needed, because for some reason the pager returns strings.
      $search_response['search_results_per_page'] = (int) $pager->getLimit();
      $search_response['search_results_count'] = (int) $pager->getTotalItems();
      $search_response['search_results_page'] = (int) $pager->getCurrentPage();
      $search_response['search_results_pages'] = (int) $pager->getTotalPages();

      $event = new SearchApiEndpointResultsAlter($search_api_endpoint, $result, $search_response, $cacheable_metadata, $request);
      $this->eventDispatcher->dispatch($event, SearchApiEndpointEvents::SEARCH_RESULTS_ALTER);
      $search_response = $event->getResponse();
      $cacheable_metadata = $event->getCacheableMetadata();

      $cacheable_metadata->addCacheContexts(['url']);
      $cacheable_metadata->addCacheTags(['search_api_list:' . $search_api_endpoint->getIndex()]);
      $cacheable_metadata->addCacheableDependency($search_api_endpoint);
    }
    catch (\Exception $e) {
      if (error_displayable()) {
        $this->messenger()->addError($e->getMessage());
      }
      $this->getLogger('search_api_decoupled')->error($e->getMessage());

      return new JsonResponse($search_response);
    }
    $response = new CacheableJsonResponse($search_response);
    $response->addCacheableDependency($cacheable_metadata);
    return $response;
  }

  /**
   * Prepares the search query.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param \Drupal\search_api_decoupled\SearchApiEndpointInterface $search_api_endpoint
   *   The object.
   *
   * @return \Drupal\search_api\Query\QueryInterface
   *   The prepared query.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\search_api\SearchApiException
   */
  protected function prepareQuery(Request $request, SearchApiEndpointInterface $search_api_endpoint) {
    /** @var \Drupal\search_api\IndexInterface $search_api_index */
    $search_api_index = $this->entityTypeManager()
      ->getStorage('search_api_index')
      ->load($search_api_endpoint->getIndex());
    $index_fields = $search_api_index->getFields();
    $limit = $search_api_endpoint->getLimit();
    $items_per_page = $request->get('limit');
    if (!empty($items_per_page) && $search_api_endpoint->isAllowedItemsPerPage($items_per_page)) {
      $limit = $items_per_page;
    }

    // Set pagination.
    $query = $search_api_index->query([
      'limit' => $limit,
      'offset' => $request->get('page') !== NULL ? $request->get('page') * $limit : 0,
    ]);

    // Set sorting.
    $sort = $request->get('sort') ?? $search_api_endpoint->getDefaultSort();
    $allowed_sorts = array_keys($index_fields);
    $allowed_sorts = array_merge($allowed_sorts, [
      'search_api_relevance',
      'search_api_datasource',
      'search_api_language',
      'search_api_id',
    ]);
    if (!in_array($sort, $allowed_sorts)) {
      $sort = $search_api_endpoint->getDefaultSort();
    }
    $order = $request->get('order') ?? $search_api_endpoint->getDefaultSortOrder();
    if (!in_array($order, ['asc', 'desc'])) {
      $order = $search_api_endpoint->getDefaultSortOrder();
    }
    $query->sort($sort, $order);

    $query->setSearchID('search_api_endpoint:' . $search_api_endpoint->id());

    /** @var \Drupal\search_api\ParseMode\ParseModeInterface $parse_mode */
    $parse_mode = $this->parseModePluginManager->createInstance($search_api_endpoint->getParseMode());
    $query->setParseMode($parse_mode);

    // Add filter for current language.
    $langcode = $this->languageManager()
      ->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)
      ->getId();
    $query->setLanguages([
      $langcode,
      LanguageInterface::LANGCODE_NOT_SPECIFIED,
    ]);

    $query->setFulltextFields($search_api_endpoint->getSearchedFields());
    $query_params = $request->query->all();
    // Add conditions for all query parameters.
    foreach ($query_params as $key => $value) {
      if (!in_array($key, array_keys($index_fields))) {
        continue;
      }
      if (in_array($key, $query->getFulltextFields())) {
        continue;
      }
      if (in_array($key, ['page', 'sort', 'order', 'q', 'f', 'operator'])) {
        continue;
      }
      // Check what operator to use.
      $operator = '=';
      // In case of multiple values.
      if (is_array($value)) {
        // If this value is a list, use IN operator.
        if (array_is_list($value)) {
          $operator = 'IN';
        }
        // If this value is an associative array, use BETWEEN operator. But
        // check that it has exactly two values.
        elseif (count($value) == 2) {
          $operator = 'BETWEEN';
          $value = array_values($value);
        }
      }
      else {
        // Check if the value is a comma separated list.
        $value = explode(',', $value);
        if (count($value) > 1) {
          $operator = 'IN';
        }
        else {
          $value = reset($value);
        }
      }
      // If the operator is explicitly set, use it.
      if (!empty($query_params['operator'][$key])) {
        $operator = SearchApiEndpoint::getConditionOperator($query_params['operator'][$key]);
      }
      $query->addCondition($key, $value, $operator);
    }

    return $query;
  }

  /**
   * Gets the url of the search result item.
   *
   * @param \Drupal\search_api\Item\ItemInterface $item
   *   The search result item.
   * @param \Drupal\Core\Cache\CacheableMetadata $cacheable_metadata
   *   The cacheable metadata.
   *
   * @return string|void
   *   The absolute url to the search result item.
   */
  protected function getSearchResultUrl(ItemInterface $item, CacheableMetadata $cacheable_metadata) {
    // Getting the url of the result item.
    [
      $datasource_type,
      $combined_id,
    ] = Utility::splitPropertyPath($item->getId(), FALSE);
    if ($datasource_type == 'entity') {
      [
        $entity_type_id,
        $entity_id_langcode,
      ] = Utility::splitCombinedId($combined_id);
      [
        $entity_id,
        $langcode,
      ] = Utility::splitPropertyPath($entity_id_langcode);
      $url = Url::fromRoute('entity.' . $entity_type_id . '.canonical', [$entity_type_id => $entity_id], [
        'language' => $this->languageManager()
          ->getLanguage($langcode),
      ])->toString(TRUE);
      $cacheable_metadata->addCacheableDependency($url);
      return $url->getGeneratedUrl();
    }
  }

}
