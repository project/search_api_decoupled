# What is this module for?

This module provides a display for Search API that allows to avoid using views
for display of search results. Search API Endpoint entities are used to define
the search endpoints and the display of the results. The purpose of this module
to use full power of Search API backends and to avoid loading Drupal entities.

Read more about Search API at https://www.drupal.org/project/search_api

# Installation

For installing this module is required to have composer-merge-plugin installed. 
Run `composer require wikimedia/composer-merge-plugin`

And update the root `composer.json` file. For example:

```
    "extra": {
        "merge-plugin": {
          "include": [
            "[web-root]/modules/contrib/search_api_decoupled/composer.libraries.json"
          ]
        },
    }
```
