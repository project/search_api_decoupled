<?php

namespace Drupal\search_api_decoupled_facets\Plugin\search_ui_element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api_decoupled_ui\ConfigurableSearchUiElementBase;

/**
 * Provides UI elements to configure facets for decoupled search.
 *
 * @SearchUiElement(
 *   id = "facet",
 *   label = @Translation("Facet"),
 * )
 */
class Facet extends ConfigurableSearchUiElementBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'facet' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    /** @var \Drupal\search_api_decoupled\SearchApiEndpointInterface $search_api_endpoint */
    $search_api_endpoint = $form_state->get('search_api_endpoint');
    $options = ['' => $this->t('- Select -')];
    foreach ($search_api_endpoint->getFacets() as $facet) {
      $options[$facet->id()] = $facet->label();
    }
    $form['facet'] = [
      '#type' => 'select',
      '#title' => $this->t('Drupal facet'),
      '#default_value' => $this->configuration['facet'],
      '#required' => TRUE,
      '#options' => $options,
    ];
    $form['label']['#title'] = $this->t('Override facet title');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['facet'] = $form_state->getValue('facet');
  }

  /**
   * {@inheritdoc}
   */
  protected function getFacet() {
    return $this->entityTypeManager->getStorage('facets_facet')->load($this->configuration['facet']);
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    if (!empty($this->configuration['facet'])) {
      $prefix = 'facets.facet.';
      $this->addDependency('config', $prefix . $this->configuration['facet']);
    }
    return $this->dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return [
      '#markup' => $this->t('Name: <a href="@url" target="_blank">@facet</a>',
        [
          '@url' => $this->getFacet()->toUrl()->toString(),
          '@facet' => $this->getFacet()->label(),
        ]),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSearchUiConfig() {
    $config = parent::getSearchUiConfig();
    $facet = $this->getFacet();
    if ($facet) {
      $widget = $facet->getWidget();
      $config['label'] = $facet->label();
      $config['widget'] = $widget['type'];
      if (empty($config['settings'])) {
        $config['settings'] = [];
      }
      $config['settings'] = array_merge($config['settings'], $widget['config']);
      if (!empty($config['settings']['label'])) {
        $config['label'] = $config['settings']['label'];
      }
      $config['settings']['use_hierarchy'] = $facet->getUseHierarchy();
      $config['settings']['expand_hierarchy'] = $facet->getExpandHierarchy();
      $config['settings']['field_alias'] = $facet->getFieldAlias();
      $config['settings']['url_alias'] = $facet->getUrlAlias();
    }
    return $config;
  }

}
