<?php

namespace Drupal\search_api_decoupled_facets\EventSubscriber;

use Drupal\Component\Plugin\Factory\FactoryInterface;
use Drupal\facets\FacetManager\DefaultFacetManager;
use Drupal\search_api\Event\QueryPreExecuteEvent;
use Drupal\search_api_decoupled\Event\SearchApiEndpointEvents;
use Drupal\search_api_decoupled\Event\SearchApiEndpointFacets;
use Drupal\search_api_decoupled\Event\SearchApiEndpointResultsAlter;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class SearchApiEndpointEventSubscriber .
 */
class SearchApiEndpointEventSubscriber implements EventSubscriberInterface {

  /**
   * The facet manager.
   *
   * @var \Drupal\facets\FacetManager\DefaultFacetManager
   */
  protected $facetManager;

  /**
   * The facets query type manager.
   *
   * @var \Drupal\Component\Plugin\Factory\FactoryInterface
   */
  protected $queryTypeManager;

  /**
   * Constructs a new SearchApiEndpointEventSubscriber object.
   *
   * @param \Drupal\facets\FacetManager\DefaultFacetManager $facet_manager
   *   The facets manager.
   * @param \Drupal\Component\Plugin\Factory\FactoryInterface $query_type_manager
   *   The query type manager.
   */
  public function __construct(DefaultFacetManager $facet_manager, FactoryInterface $query_type_manager) {
    $this->facetManager = $facet_manager;
    $this->queryTypeManager = $query_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      SearchApiEndpointEvents::SEARCH_RESULTS_ALTER => 'onSearchResultsAlter',
      SearchApiEndpointEvents::SEARCH_CONFIG_FACETS_ALTER => 'onFacetsAlter',
      'search_api.query_pre_execute' => 'queryAlter',
    ];
  }

  /**
   * Change facet configuration.
   *
   * @param \Drupal\search_api_decoupled\Event\SearchApiEndpointFacets $event
   *   The event.
   */
  public function onFacetsAlter(SearchApiEndpointFacets $event) {
    $search_api_endpoint = $event->getSearchApiEndpoint();
    $facets = $event->getFacets();
    $search_config_facets = $this->facetManager->getFacetsByFacetSourceId('search_api:search_api_endpoint__' . $search_api_endpoint->id());
    $facets = array_merge($facets, $search_config_facets);
    $event->setFacets($facets);
  }

  /**
   * Change search result response.
   *
   * @param \Drupal\search_api_decoupled\Event\SearchApiEndpointResultsAlter $event
   *   The event.
   */
  public function onSearchResultsAlter(SearchApiEndpointResultsAlter $event) {
    $search_response = $event->getResponse();
    $search_api_endpoint = $event->getSearchApiEndpoint();
    $cacheable_metadata = $event->getCacheableMetadata();
    $facets = $this->facetManager->getFacetsByFacetSourceId('search_api:search_api_endpoint__' . $search_api_endpoint->id());
    uasort($facets, function ($a, $b) {
      return $a->getWeight() <=> $b->getWeight();
    });
    foreach ($facets as $facet) {
      // Build the facet and run all processors.
      $built_facet = $this->facetManager->returnBuiltFacet($facet);
      // Process built facet results including hierarchy.
      $built_results = [];
      $this->processFacetResults($built_facet->getResults(), $built_results);
      $cacheable_metadata->addCacheableDependency($built_facet);
      $search_response['facets'][] = [
        'label' => $facet->label(),
        'key' => $facet->id(),
        'alias' => $facet->getUrlAlias(),
        'count' => count($built_facet->getResults()),
        'active_values' => $facet->getActiveItems(),
        'results' => $built_results,
      ];
    }
    $event->setResponse($search_response);
    $event->setCacheableMetadata($cacheable_metadata);
  }

  /**
   * Process facets results.
   *
   * @param \Drupal\facets\Result\Result[] $facet_results
   *   The facet results.
   * @param array $built_results
   *   Processed facet results.
   */
  protected function processFacetResults(array $facet_results, array &$built_results) {
    /** @var \Drupal\facets\Result\Result $facet_result */
    foreach ($facet_results as $facet_result) {
      $built_result = [
        'label' => $facet_result->getDisplayValue(),
        'count' => $facet_result->getCount(),
        'key' => $facet_result->getRawValue(),
        'active' => $facet_result->isActive(),
        'children' => [],
      ];
      $children = $facet_result->getChildren();
      if (!empty($children)) {
        $this->processFacetResults($children, $built_result['children']);
      }
      $built_results[] = $built_result;
    }
  }

  /**
   * Reacts to the query alter event.
   *
   * @param \Drupal\search_api\Event\QueryPreExecuteEvent $event
   *   The query alter event.
   */
  public function queryAlter(QueryPreExecuteEvent $event) {
    $query = $event->getQuery();

    if ($query->getIndex()->getServerInstance()->supportsFeature('search_api_facets')) {
      if (strpos($query->getSearchId(), 'search_api_autocomplete:') === 0) {
        $tags = $query->getTags();
        $facet_source = '';
        foreach ($tags as $tag) {
          if (strpos($tag, 'search_api_endpoint:') === 0) {
            $facet_source = 'search_api:' . str_replace(':', '__', $tag);
            break;
          }
        }
        if (!empty($facet_source)) {
          // Add the active filters.
          $this->facetManager->alterQuery($query, $facet_source);
        }
      }
    }
  }

}
