<?php

namespace Drupal\search_api_decoupled_autocomplete\Plugin\search_api_autocomplete\search;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\search_api_autocomplete\Search\SearchPluginDeriverBase;

/**
 * Derives a search plugin definition for every search config.
 *
 * @see \Drupal\search_api_decoupled_autocomplete\Plugin\search_api_autocomplete\search\SearchApiEndpoint
 */
class SearchApiEndpointDeriver extends SearchPluginDeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = [];

    try {
      $search_api_endpoint_storage = $this->getEntityTypeManager()
        ->getStorage('search_api_endpoint');
    }
    catch (PluginException $e) {
      return $this->derivatives;
    }

    /** @var \Drupal\search_api_decoupled\SearchApiEndpointInterface $search_endpoint */
    foreach ($search_api_endpoint_storage->loadMultiple() as $search_endpoint) {
      $this->derivatives[$search_endpoint->id()] = [
        'label' => $search_endpoint->label(),
        'index' => $search_endpoint->getIndex(),
      ] + $base_plugin_definition;
    }

    return $this->derivatives;
  }

}
