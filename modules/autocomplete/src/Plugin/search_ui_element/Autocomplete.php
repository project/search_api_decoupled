<?php

namespace Drupal\search_api_decoupled_autocomplete\Plugin\search_ui_element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api_decoupled_ui\Plugin\search_ui_element\SearchInputBase;

/**
 * Plugin for Search UI Element "Autocomplete".
 *
 * @SearchUiElement(
 *   id = "autocomplete",
 *   label = @Translation("Autocomplete Search Input"),
 * )
 */
class Autocomplete extends SearchInputBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    /** @var \Drupal\search_api_decoupled_ui\SearchApiEndpointUiInterface $search_api_endpoint */
    $search_api_endpoint = $form_state->get('search_api_endpoint');
    $autocomplete = $search_api_endpoint->getAutocomplete();
    if (empty($autocomplete)) {
      $form['#markup'] = $this->t('Autocomplete is not enabled for this endpoint.');
      return $form;
    }
    return $form;
  }

}
