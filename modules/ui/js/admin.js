/**
 * @file
 * Contains javascript functionality for the search api decoupled admin ui.
 */

(function (window, Drupal, once) {

  'use strict';

  Drupal.searchApiDecoupled = Drupal.searchApiDecoupled || {};

  Drupal.searchApiDecoupled.moveElement = function (evt) {
    const element = evt.item;
    element.querySelector('.search-ui-element-region').value = evt.to.getAttribute('data-region');
    element.closest('[data-region]').querySelectorAll('.search-ui-element-order-weight').forEach(function (el, index) {
      el.value = index;
    });
  }

  Drupal.behaviors.searchApiDecoupledUiAdmin = {
    attach: function (context, settings) {
      const elements = once('search-api-decoupled-ui-admin-processed', '[data-region]', context);
      elements.forEach(function (element) {
        new Sortable(element, {
          group: 'shared',
          animation: 150,
          filter: '.region-title',
          draggable: '.search-ui-element',
          onEnd: function (evt) {
            Drupal.searchApiDecoupled.moveElement(evt);
          }
        });
      });
    }
  }
})(window, Drupal, once);
