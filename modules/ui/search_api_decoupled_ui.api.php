<?php

/**
 * @file
 * Hooks provided by the Search API Decoupled UI module.
 */

/**
 * Alter the search API endpoint block render array.
 *
 * @param array $build
 *   The block render array.
 * @param array $context
 *   The context array.
 *   Consists of such elements:
 *     - 'cacheable_metadata': CacheableMetadata object to collect cacheability
 *        metadata for block render array.
 *     - 'block': The block object, to get block settings, plugin_id, etc.
 *     - 'search_api_endpoint': The search API endpoint object.
 */
function hook_sae_ui_block_alter(array &$build, array &$context) {
  if ($context['search_api_endpoint']->id() === 'default') {
    $build['#attached']['drupalSettings']['search_api_endpoint']['default']['search_input']['placeholder'] = 'Search for contents';
  }
}

/**
 * Alter the search API endpoint block render array.
 *
 * @param array $settings
 *   The search api endpoint settings.
 * @param array $context
 *   The context array.
 *   Consists of such elements:
 *     - 'cacheable_metadata': CacheableMetadata object to collect cacheability
 *        metadata for settings.
 *     - 'search_api_endpoint': The search API endpoint object.
 */
function hook_sae_ui_settings_alter(array &$settings, array &$context) {
  if ($context['search_api_endpoint']->id() === 'default') {
    $settings['elements']['829aa90e-6885-470a-b9aa-c6ec5ce57783']['settings']['placeholder'] = 'Search for contents';
  }
}
