<?php

namespace Drupal\search_api_decoupled_ui;

use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api_decoupled_ui\Plugin\search_ui_element\SearchUiElementBase;

/**
 * Abstract class providing validation and submit methods.
 */
abstract class ConfigurableSearchUiElementBase extends SearchUiElementBase implements ConfigurableSearchUiElementInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'label' => '',
      'show_label' => FALSE,
      'show_loading' => FALSE,
      'show_reset' => FALSE,
      'reset_text' => '',
      'custom_element' => '',
      'orientation' => 'horizontal',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Display Label'),
      '#description' => $this->t('The label of the element.'),
      '#default_value' => $this->configuration['label'],
    ];
    $form['show_label'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show element label'),
      '#default_value' => $this->configuration['show_label'],
    ];
    $form['show_reset'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show reset button/link for element'),
      '#default_value' => $this->configuration['show_reset'],
    ];
    $form['reset_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Reset label'),
      '#default_value' => $this->configuration['reset_text'],
      '#states' => [
        'visible' => [
          ':input[name="settings[show_reset]"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="settings[show_reset]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['show_loading'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show loading message'),
      '#default_value' => $this->configuration['show_loading'],
    ];
    $options = [
      'horizontal' => $this->t('Horizontal'),
      'vertical' => $this->t('Vertical'),
    ];
    $form['orientation'] = [
      '#type' => 'select',
      '#title' => $this->t('Orientation'),
      '#default_value' => $this->configuration['orientation'],
      '#options' => $options,
      '#description' => $this->t('Orientation of the items.'),
    ];
    // Add custom element settings.
    $form['custom_element'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom element'),
      '#description' => $this->t('The web-component tag name. Use it when you want to use your own implementation of the element.'),
      '#default_value' => $this->configuration['custom_element'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['show_loading'] = (bool) $form_state->getValue('show_loading');
    $this->configuration['show_label'] = (bool) $form_state->getValue('show_label');
    $this->configuration['show_reset'] = (bool) $form_state->getValue('show_reset');
    $this->configuration['custom_element'] = $form_state->getValue('custom_element');
    $this->configuration['label'] = $form_state->getValue('label');
    $this->configuration['reset_text'] = $form_state->getValue('reset_text');
    $this->configuration['orientation'] = $form_state->getValue('orientation');
  }

}
