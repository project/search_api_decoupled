<?php

namespace Drupal\search_api_decoupled_ui\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\search_api_decoupled\Form\SearchApiEndpointForm;
use Drupal\search_api_decoupled_ui\ConfigurableSearchUiElementInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SearchApiEndpointEditForm.
 *
 * @package Drupal\search_api_decoupled_ui\Form
 */
class SearchApiEndpointUiElementsForm extends SearchApiEndpointForm {

  /**
   * The layout plugin manager.
   *
   * @var \Drupal\Core\Layout\LayoutPluginManagerInterface
   */
  protected $layoutManager;

  /**
   * The search ui element manager.
   *
   * @var \Drupal\search_api_decoupled_ui\SearchApiDecoupledUiElementManager
   */
  protected $searchUiElementManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->layoutManager = $container->get('plugin.manager.core.layout');
    $instance->searchUiElementManager = $container->get('plugin.manager.search_api_decoupled.ui_element_manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $form['#title'] = $this->t('UI elements for %name endpoint', ['%name' => $this->entity->label()]);
    $form['#tree'] = TRUE;
    foreach ($form as $key => $element) {
      if (is_array($element) && !empty($element['#type'])) {
        $form[$key]['#access'] = FALSE;
      }
    }
    $form['state_manager'] = [
      '#type' => 'select',
      '#title' => $this->t('State manager'),
      '#options' => [
        'url' => $this->t('URL'),
        'context' => $this->t('Context'),
      ],
      '#default_value' => $this->entity->getStateManager() ?? 'url',
      '#description' => $this->t('The state manager is responsible for updating the URL and the page content when the user interacts with the search UI elements.'),
    ];
    $search_page_link = Url::fromRoute('search_api_decoupled.ui_search_page', ['search_api_endpoint' => $this->entity->id()])->toString();
    $form['search_page'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Provide search page'),
      '#default_value' => $this->entity->withSearchPage() ?? FALSE,
      '#description' => $this->t('This option provides a search page at the path <a href="' . $search_page_link . '">' . $search_page_link . '</a>'),
    ];
    $current_layout = FALSE;
    $layouts = $this->layoutManager->getGroupedDefinitions();
    $available_layouts = [];
    $filter_category = $this->t('Search')->__toString();
    if (!empty($layouts[$filter_category])) {
      foreach ($layouts[$filter_category] as $layout_id => $layout) {
        $available_layouts[$layout_id] = $layout->getLabel();
        if ($layout_id === $this->entity->getLayout()) {
          $current_layout = $layout;
        }
      }
    }
    if (empty($available_layouts)) {
      $form['layout'] = [
        '#markup' => $this->t('No layouts available. Please make sure that there is at least 1 layout with category "Search" available for this site.'),
      ];
      return $form;
    }
    $form['layout'] = [
      '#title' => $this->t('Choose a layout:'),
      '#type' => 'search_api_decoupled_layout_select',
      '#options' => $available_layouts,
      '#default_value' => $this->entity->getLayout(),
      '#required' => TRUE,
      '#attached' => [
        'library' => [
          'search_api_decoupled_ui/admin',
        ],
      ],
    ];
    if ($current_layout) {
      $form['layout']['#attached']['library'][] = $current_layout->getLibrary();
      $form['elements'] = [
        '#prefix' => '<p>' . $this->t('You can drag and drop the elements to change their order or move to another region.') . '</p>',
        '#theme' => $current_layout->getThemeHook(),
        '#layout' => $current_layout,
      ];

      foreach ($current_layout->getRegions() as $region_key => $region) {
        $form['elements']['#region_attributes'][$region_key]['class'][] = 'sortable-ui-elements';
        $form['elements'][$region_key] = [
          'title' => [
            '#prefix' => '<div class="region-title">',
            '#markup' => $region['label'],
          ],
          'button' => [
            '#type' => 'link',
            '#title' => $this->t('Place element <span class="visually-hidden">in the %region region</span>', ['%region' => $region['label']]),
            '#url' => Url::fromRoute('search_api_decoupled.ui_element_library', [
              'search_api_endpoint' => $this->entity->id(),
              'region' => $region_key,
            ]),
            '#attributes' => [
              'class' => [
                'use-ajax',
                'button',
                'button--small',
                'place-element',
              ],
              'data-dialog-type' => 'modal',
              'data-dialog-options' => Json::encode([
                'width' => 700,
              ]),
            ],
            '#suffix' => '</div>',
          ],
          '#attributes' => [
            'data-region' => $region_key,
          ],
        ];
      }
      /** @var \Drupal\search_api_decoupled_ui\SearchUiElementInterface $element */
      foreach ($this->entity->getElements() as $element) {
        $key = $element->getUuid();
        $region = $element->getRegion() ?? $this->entity->getDefaultUiElementRegion();

        $form['elements'][$region][$key] = [
          '#type' => 'details',
          '#title' => $element->label(),
          '#attributes' => [
            'class' => [
              'search-ui-element',
              'search-ui-element-' . $element->getPluginId(),
            ],
          ],
          'weight' => [
            '#type' => 'hidden',
            '#default_value' => $element->getWeight(),
            '#attributes' => [
              'class' => ['search-ui-element-order-weight'],
            ],
          ],
          'region' => [
            '#type' => 'hidden',
            '#default_value' => $region,
            '#attributes' => [
              'class' => ['search-ui-element-region'],
            ],
          ],
        ];

        $summary = $element->getSummary();

        if (!empty($summary)) {
          $summary['#prefix'] = ' ';
          $form['elements'][$region][$key]['summary'] = $summary;
        }

        $links = [];
        $is_configurable = $element instanceof ConfigurableSearchUiElementInterface;
        if ($is_configurable) {
          $links['edit'] = [
            'title' => $this->t('Edit'),
            'url' => Url::fromRoute('search_api_decoupled.ui_element_edit_form', [
              'search_api_endpoint' => $this->entity->id(),
              'ui_element' => $key,
            ]),
          ];
        }
        $links['delete'] = [
          'title' => $this->t('Delete'),
          'url' => Url::fromRoute('search_api_decoupled.ui_element_delete', [
            'search_api_endpoint' => $this->entity->id(),
            'ui_element' => $key,
          ]),
        ];

        $form['elements'][$region][$key]['#title'] = [
          [
            '#markup' => $form['elements'][$region][$key]['#title'],
          ],
          [
            '#type' => 'operations',
            '#links' => $links,
          ],
        ];
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    // Update search ui element weights.
    if (!$form_state->isValueEmpty('elements')) {
      $this->updateUiElementsWeights($form_state->getValue('elements'));
    }
    else {
      $form_state->setValue('elements', []);
    }
  }

  /**
   * Updates ui elements weights.
   *
   * @param array $elements
   *   Associative array with search ui elements having element uuid as keys and
   *   array with element settings as values.
   */
  protected function updateUiElementsWeights(array $elements) {
    $ui_element_regions = $this->entity->getUiElementRegions();
    $default_region = $this->entity->getDefaultUiElementRegion();
    foreach ($elements as $region) {
      foreach ($region as $uuid => $element_data) {
        if ($this->entity->getElements()->has($uuid)) {
          $this->entity->getElement($uuid)->setWeight($element_data['weight']);
          $this->entity->getElement($uuid)->setRegion($element_data['region']);
          $element_region = $this->entity->getElement($uuid)->getRegion();
          if (!in_array($element_region, array_keys($ui_element_regions))) {
            $this->entity->getElement($uuid)->setRegion($default_region);
          }
        }
      }
    }
  }

}
