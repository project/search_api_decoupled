<?php

namespace Drupal\search_api_decoupled_ui\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api_decoupled\SearchApiEndpointInterface;
use Drupal\search_api_decoupled_ui\SearchApiDecoupledUiElementManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an add form for search ui elements.
 *
 * @internal
 */
class SearchUiElementAddForm extends SearchUiElementFormBase {

  /**
   * The search ui elements manager.
   *
   * @var \Drupal\search_api_decoupled_ui\SearchApiDecoupledUiElementManager
   */
  protected $searchUiElementsManager;

  /**
   * Constructs a new SearchUiElementAddForm.
   *
   * @param \Drupal\search_api_decoupled_ui\SearchApiDecoupledUiElementManager $search_ui_element_manager
   *   The search ui elements manager.
   */
  public function __construct(SearchApiDecoupledUiElementManager $search_ui_element_manager) {
    $this->searchUiElementsManager = $search_ui_element_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.search_api_decoupled.ui_element_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, SearchApiEndpointInterface $search_api_endpoint = NULL, $ui_element = NULL, $region = NULL) {
    $form = parent::buildForm($form, $form_state, $search_api_endpoint, $ui_element, $region);

    $form['#title'] = $this->t('Add %label element to search endpoint config %style', [
      '%label' => $this->uiElement->label(),
      '%style' => $search_api_endpoint->label(),
    ]);
    $form['actions']['submit']['#value'] = $this->t('Add element');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareUiElement($ui_element, $region = NULL) {
    $ui_element = $this->searchUiElementsManager->createInstance($ui_element);
    // Set the initial weight so this element comes last.
    $ui_element->setWeight(count($this->searchApiEndpoint->getElements()));
    if (!empty($region)) {
      $ui_element->setRegion($region);
    }
    return $ui_element;
  }

}
