<?php

namespace Drupal\search_api_decoupled_ui\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\search_api_decoupled_ui\ConfigurableSearchUiElementInterface;
use Drupal\search_api_decoupled\SearchApiEndpointInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a base form for search ui elements.
 */
abstract class SearchUiElementFormBase extends FormBase {

  /**
   * The search endpoint.
   *
   * @var \Drupal\search_api_decoupled\SearchApiEndpointInterface
   */
  protected $searchApiEndpoint;

  /**
   * The search ui element.
   *
   * @var \Drupal\search_api_decoupled_ui\SearchUiElementInterface|\Drupal\search_api_decoupled_ui\ConfigurableSearchUiElementInterface
   */
  protected $uiElement;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'search_ui_element_form';
  }

  /**
   * Gets the search ui element.
   *
   * @return \Drupal\search_api_decoupled_ui\SearchUiElementInterface
   *   The UI Element for this form.
   */
  public function getUiElement() {
    return $this->uiElement;
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\search_api_decoupled\SearchApiEndpointInterface $search_api_endpoint
   *   The search config.
   * @param string $ui_element
   *   The search ui element.
   * @param string|null $region
   *   The region.
   *
   * @return array
   *   The form structure.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   */
  public function buildForm(array $form, FormStateInterface $form_state, SearchApiEndpointInterface $search_api_endpoint = NULL, $ui_element = NULL, $region = NULL) {
    $this->searchApiEndpoint = $search_api_endpoint;
    try {
      $this->uiElement = $this->prepareUiElement($ui_element, $region);
    }
    catch (PluginNotFoundException $e) {
      throw new NotFoundHttpException("Invalid element id: '$ui_element'.");
    }
    $request = $this->getRequest();

    if (!($this->uiElement instanceof ConfigurableSearchUiElementInterface)) {
      throw new NotFoundHttpException();
    }

    $form['uuid'] = [
      '#type' => 'value',
      '#value' => $this->uiElement->getUuid(),
    ];
    $form['id'] = [
      '#type' => 'value',
      '#value' => $this->uiElement->getPluginId(),
    ];
    $regions = $this->searchApiEndpoint->getUiElementRegions();
    $form['region'] = [
      '#type' => 'select',
      '#title' => $this->t('Region for this element'),
      '#options' => ['' => $this->t('- Select -')] + $regions,
      '#required' => TRUE,
      '#default_value' => in_array($this->uiElement->getRegion(), array_keys($regions)) ? $this->uiElement->getRegion() : $this->searchApiEndpoint->getDefaultUiElementRegion(),
    ];

    $form['settings'] = [];
    $subform_state = SubformState::createForSubform($form['settings'], $form, $form_state);
    $subform_state->set('search_api_endpoint', $this->searchApiEndpoint);
    $form['settings'] = $this->uiElement->buildConfigurationForm($form['settings'], $subform_state);
    $form['settings']['#tree'] = TRUE;

    // Check URL for weight, then search ui element, otherwise use default.
    $form['weight'] = [
      '#type' => 'hidden',
      '#value' => $request->query->has('weight') ? (int) $request->query->get('weight') : $this->uiElement->getWeight(),
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
    ];
    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => $this->searchApiEndpoint->toUrl('ui'),
      '#attributes' => ['class' => ['button']],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // The search ui element configuration is stored in the 'settings' key in
    // the form, pass that through for validation.
    $this->uiElement->validateConfigurationForm($form['settings'], SubformState::createForSubform($form['settings'], $form, $form_state));

    // Make sure weight gets saved as integer for config.
    $form_state->setValue('weight', (int) $form_state->getValue('weight'));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->cleanValues();

    // The search ui element configuration is stored in the 'settings' key in
    // the form, pass that through for submission.
    $this->uiElement->submitConfigurationForm($form['settings'], SubformState::createForSubform($form['settings'], $form, $form_state));

    $this->uiElement->setWeight($form_state->getValue('weight'));
    $this->uiElement->setRegion($form_state->getValue('region'));
    if (!$this->uiElement->getUuid()) {
      $this->searchApiEndpoint->addElement($this->uiElement->getConfiguration());
    }
    $this->searchApiEndpoint->save();

    $this->messenger()->addStatus($this->t('The ui element was successfully added.'));
    $form_state->setRedirectUrl($this->searchApiEndpoint->toUrl('ui'));
  }

  /**
   * Converts a search ui element ID into an object.
   *
   * @param string $ui_element
   *   The search ui element ID.
   * @param string|null $region
   *   The region.
   *
   * @return \Drupal\search_api_decoupled_ui\SearchUiElementInterface
   *   The search ui element object.
   */
  abstract protected function prepareUiElement($ui_element, $region = NULL);

}
