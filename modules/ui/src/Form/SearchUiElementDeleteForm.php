<?php

namespace Drupal\search_api_decoupled_ui\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api_decoupled\SearchApiEndpointInterface;

/**
 * Form for deleting a search ui element.
 *
 * @internal
 */
class SearchUiElementDeleteForm extends ConfirmFormBase {

  /**
   * The search endpoint.
   *
   * @var \Drupal\search_api_decoupled\SearchApiEndpointInterface
   */
  protected $searchApiEndpoint;

  /**
   * The search ui element to be deleted.
   *
   * @var \Drupal\search_api_decoupled_ui\SearchUiElementInterface
   */
  protected $uiElement;

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the @element element from the %label endpoint?',
      [
        '%lebel' => $this->searchApiEndpoint->label(),
        '@element' => $this->uiElement->label(),
      ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->searchApiEndpoint->toUrl('edit-form');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'search_ui_element_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, SearchApiEndpointInterface $search_api_endpoint = NULL, $ui_element = NULL) {
    $this->searchApiEndpoint = $search_api_endpoint;
    $this->uiElement = $this->searchApiEndpoint->getElement($ui_element);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->searchApiEndpoint->deleteElement($this->uiElement);
    $this->messenger()->addStatus($this->t('The search ui element %name has been deleted.', ['%name' => $this->uiElement->label()]));
    $form_state->setRedirectUrl($this->searchApiEndpoint->toUrl('ui'));
  }

}
