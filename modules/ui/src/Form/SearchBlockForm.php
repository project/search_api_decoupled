<?php

namespace Drupal\search_api_decoupled_ui\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Builds the search form for the search block.
 *
 * @internal
 */
class SearchBlockForm extends FormBase {

  use StringTranslationTrait;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new SearchBlockForm.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $entity_type_manager) {
    $this->moduleHandler = $module_handler;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'search_api_decoupled_ui_search_block_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $search_api_endpoint = NULL, $configuration = []) {
    if (!empty($search_api_endpoint)) {
      /** @var \Drupal\search_api_decoupled\SearchApiEndpointInterface $endpoint */
      $endpoint = $this->entityTypeManager->getStorage('search_api_endpoint')->load($search_api_endpoint);

      $form_state
        ->setMethod('GET')
        ->setAlwaysProcess(TRUE)
        ->setCached(FALSE)
        ->disableRedirect();

      $form['#cache'] = [
        'max-age' => 0,
      ];
      if (!empty($configuration['redirect_path'])) {
        $url = Url::fromUserInput($configuration['redirect_path']);
      }
      else {
        $url = Url::fromRoute('<current>');
      }
      $form['#action'] = $url->toString();
      $form['q'] = [
        '#type' => 'search',
        '#title' => $this->t('Search'),
        '#title_display' => 'invisible',
        '#placeholder' => $configuration['placeholder'] ?? NULL,
      ];

      $form['actions'] = [
        '#type' => 'actions',
      ];
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Search'),
        // Prevent op from showing up in the query string.
        '#name' => '',
      ];

      if (!empty($configuration['autocomplete']) && $endpoint->getAutocomplete()) {
        $form['q']['#type'] = 'search_api_autocomplete';
        $form['q']['#search_id'] = $search_api_endpoint;

        if ($endpoint->getAutocomplete()->getOption('autosubmit')) {
          $form['q']['#autosubmit'] = TRUE;
          $form['actions']['submit']['#attributes']['class'][] = 'visually-hidden';
        }
      }

      // Add an after build step.
      $form['#after_build'][] = [get_class($this), 'afterBuild'];
    }

    return $form;
  }

  /**
   * After build callback.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The form.
   */
  public static function afterBuild(array $form, FormStateInterface $form_state) {
    unset($form['form_token']);
    unset($form['form_build_id']);
    unset($form['form_id']);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

}
