<?php

namespace Drupal\search_api_decoupled_ui\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api_decoupled\SearchApiEndpointInterface;

/**
 * Provides an edit form for search ui elements.
 *
 * @internal
 */
class SearchUiElementEditForm extends SearchUiElementFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, SearchApiEndpointInterface $search_api_endpoint = NULL, $ui_element = NULL, $region = NULL) {
    $form = parent::buildForm($form, $form_state, $search_api_endpoint, $ui_element, $region);

    $form['#title'] = $this->t('Edit %label element to endpoint %label', [
      '%label' => $this->uiElement->label(),
      '%label' => $search_api_endpoint->label(),
    ]);
    $form['actions']['submit']['#value'] = $this->t('Update element');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareUiElement($ui_element, $region = NULL) {
    return $this->searchApiEndpoint->getElement($ui_element);
  }

}
