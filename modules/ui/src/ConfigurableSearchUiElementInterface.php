<?php

namespace Drupal\search_api_decoupled_ui;

use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Interface for search UI elements class for search api configuration.
 *
 * @package Drupal\search_api_decoupled_ui
 */
interface ConfigurableSearchUiElementInterface extends SearchUiElementInterface, PluginFormInterface {

}
