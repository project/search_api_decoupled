<?php

namespace Drupal\search_api_decoupled_ui;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for search ui element plugins.
 */
interface SearchUiElementInterface extends PluginInspectionInterface, ConfigurableInterface, DependentPluginInterface {

  /**
   * Returns a render array summarizing the configuration of the ui element.
   *
   * @return array
   *   A render array.
   */
  public function getSummary();

  /**
   * Returns the search ui element label.
   *
   * @return string
   *   The search ui element label.
   */
  public function label();

  /**
   * Returns the unique ID representing the search ui element.
   *
   * @return string
   *   The search ui element ID.
   */
  public function getUuid();

  /**
   * Returns the weight of the search ui element.
   *
   * @return int|string
   *   Either the integer weight of the search ui element, or an empty string.
   */
  public function getWeight();

  /**
   * Sets the weight for this search ui element.
   *
   * @param int $weight
   *   The weight for this search ui element.
   *
   * @return $this
   */
  public function setWeight($weight);

  /**
   * Gets the search ui element region.
   *
   * @return string
   *   The region.
   */
  public function getRegion();

  /**
   * Sets the search ui element region.
   *
   * @param string $region
   *   The region.
   *
   * @return $this
   */
  public function setRegion($region);

  /**
   * Gets the config for search ui.
   *
   * @return array
   *   The search ui config.
   */
  public function getSearchUiConfig();

}
