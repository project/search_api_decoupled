<?php

namespace Drupal\search_api_decoupled_ui\Entity;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use Drupal\Core\Url;
use Drupal\search_api_decoupled\Entity\SearchApiEndpoint as BaseSearchApiEndpoint;
use Drupal\search_api_decoupled_ui\SearchApiEndpointUiInterface;
use Drupal\search_api_decoupled_ui\SearchUiElementInterface;
use Drupal\search_api_decoupled_ui\SearchUiElementsPluginCollection;

/**
 * Defines overriden search api endpoint entity class with UI elements.
 */
class SearchApiEndpoint extends BaseSearchApiEndpoint implements SearchApiEndpointUiInterface, EntityWithPluginCollectionInterface {

  /**
   * The layout.
   *
   * @var string
   */
  protected $layout = 'search_onecol';

  /**
   * The array of ui elements for this search endpoint.
   *
   * @var array
   */
  protected $elements = [];

  /**
   * Holds the collection of search ui elements used by this search endpoint.
   *
   * @var \Drupal\search_api_decoupled_ui\SearchUiElementsPluginCollection
   */
  protected $elementsCollection;

  /**
   * The state manager.
   *
   * @var string
   */
  protected $state_manager = 'url';

  /**
   * Whether dedicated search page should be provided.
   *
   * @var bool
   */
  protected $search_page = FALSE;

  /**
   * Returns the ui elements plugin manager.
   *
   * @return \Drupal\Component\Plugin\PluginManagerInterface
   *   The ui elements plugin manager.
   */
  protected function getUiElementsPluginManager() {
    return \Drupal::service('plugin.manager.search_api_decoupled.ui_element_manager');
  }

  /**
   * {@inheritdoc}
   */
  public function getElement($element) {
    return $this->getElements()->get($element);
  }

  /**
   * {@inheritdoc}
   */
  public function getElements() {
    if (!$this->elementsCollection) {
      $this->elementsCollection = new SearchUiElementsPluginCollection($this->getUiElementsPluginManager(), $this->elements);
      $this->elementsCollection->sort();
    }
    return $this->elementsCollection;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    return ['elements' => $this->getElements()];
  }

  /**
   * {@inheritdoc}
   */
  public function addElement(array $configuration) {
    $configuration['uuid'] = $this->uuidGenerator()->generate();
    $configuration['search_api_endpoint'] = $this->id();
    $this->getElements()->addInstanceId($configuration['uuid'], $configuration);
    return $configuration['uuid'];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteElement(SearchUiElementInterface $element) {
    $this->getElements()->removeInstanceId($element->getUuid());
    $this->save();
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getUiElementRegions() {
    $layout = \Drupal::service('plugin.manager.core.layout')->getDefinition($this->getLayout());
    $regions = [];
    foreach ($layout->getRegions() as $key => $region) {
      $regions[$key] = $region['label'];
    }
    return $regions;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultUiElementRegion() {
    $layout = \Drupal::service('plugin.manager.core.layout')->getDefinition($this->getLayout());
    return $layout->getDefaultRegion();
  }

  /**
   * {@inheritdoc}
   */
  public function getLayout() {
    return $this->layout;
  }

  /**
   * {@inheritdoc}
   */
  public function getStateManager() {
    return $this->state_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function setStateManager($state_manager) {
    $this->state_manager = $state_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function withSearchPage() {
    return $this->search_page;
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailableFields() {
    $indexed_fields = $this->getIndexedFields();
    $excluded_fields = $this->getExcludedFields();
    if (!empty($indexed_fields) && !empty($excluded_fields)) {
      return array_diff_key($indexed_fields, $excluded_fields);
    }
    return $indexed_fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getUiSettings(CacheableMetadata $cacheable_metadata = NULL, $absolute_urls = FALSE) {
    $cacheable_metadata = $cacheable_metadata ?: new CacheableMetadata();
    $regions = [];
    $search_elements = [];
    $indexed_fields = $this->getIndexFieldsDefinitions();
    $ui_regions = $this->getUiElementRegions();
    foreach (array_keys($ui_regions) as $region) {
      $regions[$region] = [];
      /** @var \Drupal\search_api_decoupled_ui\SearchUiElementInterface $element */
      foreach ($this->getElements() as $element) {
        if ($element->getRegion() == $region) {
          $regions[$region][$element->getWeight()] = $element->getUuid();
        }
        if (empty($search_elements[$element->getUuid()])) {
          $search_ui_config = $element->getSearchUiConfig();
          // For exposed filter UI client expects the field type to be exposed.
          if ($element->getPluginId() == 'exposed_filter') {
            $search_ui_config['settings']['field_type'] = $indexed_fields[$search_ui_config['settings']['field_name']]->getType();
          }
          $search_elements[$element->getUuid()] = $search_ui_config;
        }
      }
    }
    foreach ($regions as $region => $elements) {
      $regions[$region] = array_values($elements);
    }
    $base_url = $this->getBaseUrl()->setAbsolute($absolute_urls)->toString(TRUE);
    $cacheable_metadata->addCacheableDependency($base_url);
    $settings = [
      'id' => $this->id(),
      'label' => $this->label(),
      'base_url' => $base_url->getGeneratedUrl(),
      'elements' => $search_elements,
      'items_per_page_options' => $this->getItemsPerPage(),
      'default_sort' => $this->getDefaultSort(),
      'default_sort_order' => $this->getDefaultSortOrder(),
      'layout' => [
        'id' => $this->getLayout(),
        'regions' => $regions,
      ],
      'state_manager' => $this->getStateManager(),
    ];
    if ($autocomplete = $this->getAutocomplete()) {
      $autocomplete_url = Url::fromRoute('search_api_autocomplete.autocomplete', ['search_api_autocomplete_search' => $autocomplete->id()])->setAbsolute($absolute_urls)->toString(TRUE);
      $settings['base_url_autocomplete'] = $autocomplete_url->getGeneratedUrl();
      $cacheable_metadata->addCacheableDependency($autocomplete_url);
      $cacheable_metadata->addCacheableDependency($autocomplete);
    }
    $cacheable_metadata->addCacheableDependency($this);
    return $settings;
  }

}
