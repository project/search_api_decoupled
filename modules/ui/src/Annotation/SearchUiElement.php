<?php

namespace Drupal\search_api_decoupled_ui\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a search ui element annotation object.
 *
 * Plugin Namespace: Plugin\search_ui_element.
 *
 * @Annotation
 */
class SearchUiElement extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the search ui element.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A brief description of the search ui element.
   *
   * This property is optional and it does not need to be declared.
   *
   * This will be shown when adding or configuring this search ui element.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description = '';

}
