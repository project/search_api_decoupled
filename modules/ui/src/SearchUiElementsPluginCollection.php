<?php

namespace Drupal\search_api_decoupled_ui;

use Drupal\Core\Plugin\DefaultLazyPluginCollection;

/**
 * A collection of search ui elements.
 */
class SearchUiElementsPluginCollection extends DefaultLazyPluginCollection {

  /**
   * {@inheritdoc}
   */
  public function sortHelper($aID, $bID) {
    return $this->get($aID)->getWeight() <=> $this->get($bID)->getWeight();
  }

}
