<?php

namespace Drupal\search_api_decoupled_ui\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Http\Exception\CacheableNotFoundHttpException;
use Drupal\search_api_decoupled_ui\SearchApiEndpointUiInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SearchPage for dedicated "coupled" search page.
 */
class SearchPage extends ControllerBase {

  /**
   * The block manager.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected $blockManager;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->blockManager = $container->get('plugin.manager.block');
    return $instance;
  }

  /**
   * Gets the dedicated search page title.
   *
   * @param \Drupal\search_api_decoupled_ui\SearchApiEndpointUiInterface $search_api_endpoint
   *   The search api endpoint.
   *
   * @return string
   *   The page title.
   */
  public function title(SearchApiEndpointUiInterface $search_api_endpoint) {
    if ($search_api_endpoint->withSearchPage()) {
      return $search_api_endpoint->label();
    }
    return $this->t('Page not found');
  }

  /**
   * Gets the dedicated search page content.
   *
   * @param \Drupal\search_api_decoupled_ui\SearchApiEndpointUiInterface $search_api_endpoint
   *   The search api endpoint.
   *
   * @return array
   *   The page content as render array.
   */
  public function content(SearchApiEndpointUiInterface $search_api_endpoint) {
    if (!$search_api_endpoint->withSearchPage()) {
      throw new CacheableNotFoundHttpException($search_api_endpoint);
    }
    $search_block = $this->blockManager->createInstance('search_api_endpoint:' . $search_api_endpoint->id());
    return [
      '#theme' => 'block',
      '#configuration' => $search_block->getConfiguration(),
      '#plugin_id' => $search_block->getPluginId(),
      '#base_plugin_id' => $search_block->getBaseId(),
      '#derivative_plugin_id' => $search_block->getDerivativeId(),
      'content' => $search_block->build(),
    ];
  }

}
