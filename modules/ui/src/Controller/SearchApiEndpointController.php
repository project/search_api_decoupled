<?php

namespace Drupal\search_api_decoupled_ui\Controller;

use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Controller\ControllerBase;
use Drupal\search_api_decoupled\SearchApiEndpointInterface;

/**
 * Controller routines for search api decoupled ui.
 */
class SearchApiEndpointController extends ControllerBase {

  /**
   * Returns the UI settings for a search endpoint.
   *
   * @param \Drupal\search_api_decoupled\SearchApiEndpointInterface $search_api_endpoint
   *   The search endpoint.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The UI settings.
   */
  public function uiSettings(SearchApiEndpointInterface $search_api_endpoint) {
    $cacheable_metadata = new CacheableMetadata();
    $settings = $search_api_endpoint->getUiSettings($cacheable_metadata, TRUE);
    $context = [
      'cacheable_metadata' => $cacheable_metadata,
      'search_api_endpoint' => $search_api_endpoint,
    ];
    $this->moduleHandler()->alter('sae_ui_settings', $settings, $context);
    $response = new CacheableJsonResponse($settings);
    $response->addCacheableDependency($cacheable_metadata);
    return $response;
  }

}
