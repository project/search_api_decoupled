<?php

namespace Drupal\search_api_decoupled_ui\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Plugin\PluginFormFactoryInterface;
use Drupal\Core\Url;
use Drupal\search_api_decoupled\SearchApiEndpointInterface;
use Drupal\search_api_decoupled_ui\SearchApiDecoupledUiElementManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller routines for search api decoupled ui.
 */
class SearchUiElementLibrary extends ControllerBase {

  /**
   * The search ui element manager.
   *
   * @var \Drupal\search_api_decoupled_ui\SearchApiDecoupledUiElementManager
   */
  protected $searchUiElementManager;

  /**
   * The plugin form manager.
   *
   * @var \Drupal\Core\Plugin\PluginFormFactoryInterface
   */
  protected $pluginFormFactory;

  /**
   * SearchUiElementLibrary constructor.
   *
   * @param \Drupal\search_api_decoupled_ui\SearchApiDecoupledUiElementManager $search_ui_element_manager
   *   The search ui element manager.
   * @param \Drupal\Core\Plugin\PluginFormFactoryInterface $plugin_form_manager
   *   The plugin form manager.
   */
  public function __construct(SearchApiDecoupledUiElementManager $search_ui_element_manager, PluginFormFactoryInterface $plugin_form_manager) {
    $this->searchUiElementManager = $search_ui_element_manager;
    $this->pluginFormFactory = $plugin_form_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.search_api_decoupled.ui_element_manager'),
      $container->get('plugin_form.factory')
    );
  }

  /**
   * Shows a list of UI elements that can be added to a layout region.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   * @param \Drupal\search_api_decoupled\SearchApiEndpointInterface $search_api_endpoint
   *   The search api endpoint object.
   * @param string $region
   *   The name of the region to show elements for.
   *
   * @return array
   *   A render array as expected by the renderer.
   */
  public function list(Request $request, SearchApiEndpointInterface $search_api_endpoint, $region = '') {
    $headers = [
      ['data' => $this->t('UI Element')],
      ['data' => $this->t('Operations')],
    ];
    if (empty($region)) {
      $region = $search_api_endpoint->getDefaultUiElementRegion();
    }

    $definitions = $this->searchUiElementManager->getDefinitions();
    uasort($definitions, function ($a, $b) {
      return Unicode::strcasecmp($a['label'], $b['label']);
    });

    $rows = [];
    foreach ($definitions as $plugin_id => $plugin_definition) {
      $row = [];
      $row['title']['data'] = [
        '#type' => 'inline_template',
        '#template' => '<div class="block-filter-text-source">{{ label }}</div>',
        '#context' => [
          'label' => $plugin_definition['label'],
        ],
      ];
      $links['add'] = [
        'title' => $this->t('Place element'),
        'url' => Url::fromRoute('search_api_decoupled.ui_element_add_form', [
          'search_api_endpoint' => $search_api_endpoint->id(),
          'ui_element' => $plugin_id,
          'region' => $region,
        ]),
        'attributes' => [
          'class' => ['use-ajax'],
          'data-dialog-type' => 'modal',
          'data-dialog-options' => Json::encode([
            'width' => 700,
          ]),
        ],
      ];
      $row['operations']['data'] = [
        '#type' => 'operations',
        '#links' => $links,
      ];
      $rows[] = $row;
    }

    $build['#attached']['library'][] = 'block/drupal.block.admin';

    $build['filter'] = [
      '#type' => 'search',
      '#title' => $this->t('Filter'),
      '#title_display' => 'invisible',
      '#size' => 30,
      '#placeholder' => $this->t('Filter by element name'),
      '#attributes' => [
        'class' => ['block-filter-text'],
        'data-element' => '.block-add-table',
        'title' => $this->t('Enter a part of the element name to filter by.'),
      ],
    ];

    $build['blocks'] = [
      '#type' => 'table',
      '#header' => $headers,
      '#rows' => $rows,
      '#empty' => $this->t('No elements available.'),
      '#attributes' => [
        'class' => ['block-add-table'],
      ],
    ];

    return $build;
  }

}
