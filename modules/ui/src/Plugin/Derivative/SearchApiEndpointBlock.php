<?php

namespace Drupal\search_api_decoupled_ui\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SearchApiEndpointBlock contains deriver for facets source.
 *
 * @package Drupal\search_api_decoupled_ui\Plugin\Derivative
 */
class SearchApiEndpointBlock extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * SearchApiEndpointBlock constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach ($this->entityTypeManager->getStorage('search_api_endpoint')->loadMultiple() as $id => $search_api_endpoint) {
      $this->derivatives[$id] = $base_plugin_definition;
      $this->derivatives[$id]['admin_label'] = $base_plugin_definition['admin_label'] . ': ' . $search_api_endpoint->label();
      $this->derivatives[$id]['config_dependencies']['config'] = ['search_api_decoupled.search_api_endpoint.' . $id];
    }
    return $this->derivatives;
  }

}
