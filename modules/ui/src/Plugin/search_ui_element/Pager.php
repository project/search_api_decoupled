<?php

namespace Drupal\search_api_decoupled_ui\Plugin\search_ui_element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api_decoupled_ui\ConfigurableSearchUiElementBase;

/**
 * Provide configuration UI for search result pager.
 *
 * @SearchUiElement(
 *   id = "pager",
 *   label = @Translation("Pager"),
 * )
 */
class Pager extends ConfigurableSearchUiElementBase {

  const WIDGETS = [
    'full' => 'Full',
    'mini' => 'Mini',
    'loadmore' => 'Load more',
  ];

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'widget' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['widget'] = [
      '#type' => 'select',
      '#title' => $this->t('Pager type'),
      '#default_value' => $this->configuration['widget'],
      '#required' => TRUE,
      '#options' => [
        '' => $this->t('- Select -'),
        'full' => $this->t('Full'),
        'mini' => $this->t('Mini'),
        'loadmore' => $this->t('Load more (deprecated, use "Pager (load more)" instead)'),
      ],
    ];
    $form['show_reset']['#access'] = FALSE;
    $form['reset_text']['#access'] = FALSE;
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['widget'] = $form_state->getValue('widget');
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return [
      '#markup' => $this->t('Widget: @widget.', ['@widget' => static::WIDGETS[$this->configuration['widget']]]),
    ];
  }

}
