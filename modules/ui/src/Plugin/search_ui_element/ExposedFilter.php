<?php

namespace Drupal\search_api_decoupled_ui\Plugin\search_ui_element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api_decoupled\Entity\SearchApiEndpoint;
use Drupal\search_api_decoupled_ui\ConfigurableSearchUiElementBase;

/**
 * Provide configuration UI for exposed filter.
 *
 * @SearchUiElement(
 *   id = "exposed_filter",
 *   label = @Translation("Exposed Filter"),
 * )
 */
class ExposedFilter extends ConfigurableSearchUiElementBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'field_name' => '',
      'show_label' => TRUE,
      'reset_text' => 'Reset',
      'show_reset' => TRUE,
      'orientation' => 'vertical',
      'operator' => 'equal',
      'default_value' => '',
      'default_value_2' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    /** @var \Drupal\search_api_decoupled\SearchApiEndpointInterface $search_api_endpoint */
    $search_api_endpoint = $form_state->get('search_api_endpoint');
    $available_fields = $search_api_endpoint->getIndexedFields();
    $options = ['' => $this->t('- Select -')];
    foreach ($available_fields as $field_name => $field_label) {
      $options[$field_name] = $field_label;
    }
    $form['field_name'] = [
      '#type' => 'select',
      '#title' => $this->t('Field to filter'),
      '#default_value' => $this->configuration['field_name'],
      '#required' => TRUE,
      '#options' => $options,
      '#weight' => 1,
    ];
    $form['operator'] = [
      '#type' => 'select',
      '#title' => $this->t('Operator'),
      '#default_value' => $this->configuration['operator'],
      '#required' => TRUE,
      '#options' => SearchApiEndpoint::getOperators(),
    ];
    $form['default_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default value'),
      '#default_value' => $this->configuration['default_value'],
      '#description' => $this->t('The default value for the filter element. In case of multiple values, provide comma separated string.'),
    ];
    $form['default_value_2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default value range (max)'),
      '#default_value' => $this->configuration['default_value_2'],
      '#description' => $this->t('For "between" operator 2 values are needed.'),
      '#states' => [
        'visible' => [
          ':input[name="settings[operator]"]' => ['value' => 'between'],
        ],
      ],
    ];
    $form['show_loading']['#weight'] = 5;
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['field_name'] = $form_state->getValue('field_name');
    $this->configuration['operator'] = $form_state->getValue('operator');
    $this->configuration['default_value'] = $form_state->getValue('default_value');
    $this->configuration['default_value_2'] = $form_state->getValue('default_value_2');
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $operators = SearchApiEndpoint::getOperators();
    return [
      '#markup' => $this->t('Filter field: "@field" with operator "@operator". Has default value: @default_value', [
        '@field' => $this->configuration['field_name'],
        '@operator' => $operators[$this->configuration['operator']],
        '@default_value' => $this->configuration['default_value'] ? $this->t('Yes') : $this->t('No'),
      ]),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSearchUiConfig() {
    $config = parent::getSearchUiConfig();
    if ($config['settings']['show_label'] && !empty($config['settings']['label'])) {
      $config['label'] = $config['settings']['label'];
    }
    if (isset($config['settings']['label'])) {
      unset($config['settings']['label']);
    }
    if (in_array($config['settings']['operator'], ['in', 'not_in'])) {
      $config['settings']['default_value'] = explode(',', $config['settings']['default_value']);
    }
    return $config;
  }

}
