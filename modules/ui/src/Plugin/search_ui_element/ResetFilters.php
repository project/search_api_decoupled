<?php

namespace Drupal\search_api_decoupled_ui\Plugin\search_ui_element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api_decoupled_ui\ConfigurableSearchUiElementBase;

/**
 * Provide configuration form for reset filters.
 *
 * @SearchUiElement(
 *   id = "reset_filters",
 *   label = @Translation("Reset all filters"),
 * )
 */
class ResetFilters extends ConfigurableSearchUiElementBase {

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return [
      '#markup' => $this->t('Display label: @label.', ['@label' => $this->configuration['label']]),
    ];
  }

}
