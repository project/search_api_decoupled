<?php

namespace Drupal\search_api_decoupled_ui\Plugin\search_ui_element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api_decoupled_ui\ConfigurableSearchUiElementBase;

/**
 * Provide configuration UI for custom search.
 *
 * @SearchUiElement(
 *   id = "search_result_custom",
 *   label = @Translation("Custom search result"),
 * )
 */
class SearchResultLocal extends ConfigurableSearchUiElementBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'columns' => 1,
      'no_results' => $this->t('No results found.'),
      'loading' => $this->t('Loading...'),
      'field_title' => '',
      'field_category' => '',
      'field_text' => '',
      'field_image' => '',
      'field_date' => '',
      'show_switch' => TRUE,
      'show_loading' => TRUE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $search_api_endpoint = $form_state->get('search_api_endpoint');
    $available_fields = $search_api_endpoint->getAvailableFields();
    $options = ['' => $this->t('- Select -')];
    foreach ($available_fields as $field_name => $field_label) {
      $options[$field_name] = $field_label;
    }
    $form['field_title'] = [
      '#type' => 'select',
      '#title' => $this->t('Title of search result'),
      '#default_value' => $this->configuration['field_title'],
      '#required' => TRUE,
      '#options' => $options,
      '#weight' => 1,
    ];
    $form['field_text'] = [
      '#type' => 'select',
      '#title' => $this->t('Text of search result'),
      '#default_value' => $this->configuration['field_text'],
      '#required' => TRUE,
      '#options' => $options,
      '#weight' => 1,
    ];
    $form['field_category'] = [
      '#type' => 'select',
      '#title' => $this->t('Category/type/tag of search result'),
      '#default_value' => $this->configuration['field_category'],
      '#options' => $options,
      '#weight' => 1,
    ];
    $form['field_image'] = [
      '#type' => 'select',
      '#title' => $this->t('Image of search result'),
      '#default_value' => $this->configuration['field_image'],
      '#options' => $options,
      '#weight' => 1,
    ];
    $form['field_date'] = [
      '#type' => 'select',
      '#title' => $this->t('Date of search result'),
      '#default_value' => $this->configuration['field_date'],
      '#required' => FALSE,
      '#options' => $options,
      '#weight' => 1,
    ];
    $form['columns'] = [
      '#type' => 'number',
      '#title' => $this->t('Columns'),
      '#default_value' => $this->configuration['columns'],
      '#min' => 1,
      '#max' => 4,
      '#weight' => 3,
    ];
    $form['no_results'] = [
      '#type' => 'textfield',
      '#title' => $this->t('No results message'),
      '#default_value' => $this->configuration['no_results'],
      '#weight' => 4,
    ];
    $form['show_loading']['#weight'] = 5;
    $form['loading'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Loading message'),
      '#default_value' => $this->configuration['loading'],
      '#weight' => 6,
    ];
    $form['show_switch'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show switch'),
      '#default_value' => $this->configuration['show_switch'],
      '#weight' => 7,
    ];
    $form['show_reset']['#access'] = FALSE;
    $form['reset_text']['#access'] = FALSE;
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['field_title'] = $form_state->getValue('field_title');
    $this->configuration['field_text'] = $form_state->getValue('field_text');
    $this->configuration['field_category'] = $form_state->getValue('field_category');
    $this->configuration['field_image'] = $form_state->getValue('field_image');
    $this->configuration['field_date'] = $form_state->getValue('field_date');
    $this->configuration['columns'] = $form_state->getValue('columns');
    $this->configuration['no_results'] = $form_state->getValue('no_results');
    $this->configuration['loading'] = $form_state->getValue('loading');
    $this->configuration['show_switch'] = (bool) $form_state->getValue('show_switch');
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return [
      '#markup' => $this->t('Title: @field_title, <br/> Text: @field_text, <br/> Category: @field_category, <br/> Image: @field_image, <br/> Column(s): @columns, <br/> Show switch: @show_switch, <br/> Show loading: @show_loading, <br/> Loading text: @loading', [
        '@field_title' => $this->configuration['field_title'],
        '@field_text' => $this->configuration['field_text'],
        '@field_category' => $this->configuration['field_category'],
        '@field_image' => $this->configuration['field_image'],
        '@field_date' => $this->configuration['field_date'],
        '@columns' => $this->configuration['columns'],
        '@loading' => $this->configuration['loading'],
        '@show_switch' => $this->configuration['show_switch'] ? $this->t('Yes') : $this->t('No'),
        '@show_loading' => $this->configuration['show_loading'] ? $this->t('Yes') : $this->t('No'),
      ]),
    ];
  }

}
