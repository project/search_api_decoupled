<?php

namespace Drupal\search_api_decoupled_ui\Plugin\search_ui_element;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provide UI for configuring the search result display.
 *
 * @SearchUiElement(
 *   id = "search_result_switch",
 *   label = @Translation("Rendered search result list or grid"),
 * )
 */
class SearchResultSwitch extends SearchResult {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['field_grid' => ''] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['field_grid'] = [
      '#type' => 'select',
      '#title' => $this->t('Field with rendered search result as card'),
      '#default_value' => $this->configuration['field_grid'],
      '#required' => TRUE,
      '#options' => $form['field']['#options'],
      '#weight' => 2,
    ];
    $form['show_switch'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show switch'),
      '#default_value' => $this->configuration['show_switch'],
      '#weight' => 7,
    ];
    $form['show_reset']['#access'] = FALSE;
    $form['reset_text']['#access'] = FALSE;
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['field_grid'] = $form_state->getValue('field_grid');
    $this->configuration['show_switch'] = (bool) $form_state->getValue('show_switch');
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return [
      '#markup' => $this->t('Field with HTML for list: @field, for grid: @field_grid in @columns column(s)', [
        '@field' => $this->configuration['field'],
        '@field_grid' => $this->configuration['field_grid'],
        '@columns' => $this->configuration['columns'],
      ]),
    ];
  }

}
