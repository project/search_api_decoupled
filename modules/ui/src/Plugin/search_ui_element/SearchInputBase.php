<?php

namespace Drupal\search_api_decoupled_ui\Plugin\search_ui_element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api_decoupled_ui\ConfigurableSearchUiElementBase;

/**
 * Base class for search input elements.
 */
class SearchInputBase extends ConfigurableSearchUiElementBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'placeholder' => $this->t('Search for contents'),
      'no_empty' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Placeholder'),
      '#default_value' => $this->configuration['placeholder'],
    ];
    $form['no_empty'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Do not perform request if input is empty'),
      '#default_value' => !empty($this->configuration['no_empty']),
      '#description' => $this->t('If checked, the search request will not be performed if the input is empty and no search results will be shown, including facets.'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['placeholder'] = $form_state->getValue('placeholder');
    $this->configuration['no_empty'] = (bool) $form_state->getValue('no_empty');
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return [
      '#markup' => $this->t('Placeholder: @placeholder.',
        [
          '@placeholder' => $this->configuration['placeholder'],
        ]),
    ];
  }

}
