<?php

namespace Drupal\search_api_decoupled_ui\Plugin\search_ui_element;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provide UI for search configuration.
 *
 * @SearchUiElement(
 *   id = "search_input",
 *   label = @Translation("Search input"),
 * )
 */
class SearchInput extends SearchInputBase {

  const WIDGETS = [
    'default' => 'Default',
    'autocomplete' => 'Autocomplete (deprecated, use "Autocomplete Search Input" instead)',
  ];

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'widget' => 'default',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $search_api_endpoint = $form_state->get('search_api_endpoint');
    $autocomplete = $search_api_endpoint->getAutocomplete();
    $options = static::WIDGETS;
    if (empty($autocomplete)) {
      unset($options['autocomplete']);
    }
    $form['widget'] = [
      '#type' => 'select',
      '#title' => $this->t('Search input type'),
      '#default_value' => $this->configuration['widget'],
      '#required' => TRUE,
      '#options' => [
        '' => $this->t('- Select -'),
      ] + $options,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['widget'] = $form_state->getValue('widget');
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return [
      '#markup' => $this->t('Widget: @widget. Placeholder: @placeholder.',
        [
          '@widget' => static::WIDGETS[$this->configuration['widget']],
          '@placeholder' => $this->configuration['placeholder'],
        ]),
    ];
  }

}
