<?php

namespace Drupal\search_api_decoupled_ui\Plugin\search_ui_element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api_decoupled_ui\ConfigurableSearchUiElementBase;

/**
 * Provides a search UI element for sorting.
 *
 * @SearchUiElement(
 *   id = "sorting",
 *   label = @Translation("Sorting"),
 *   description = @Translation("Displays the sorting options."),
 * )
 */
class Sorting extends ConfigurableSearchUiElementBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'info' => [],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Element title'),
      '#default_value' => $this->configuration['label'] ?? '',
    ];
    $form['show_label'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show title'),
      '#default_value' => !empty($this->configuration['show_label']),
      '#states' => [
        'visible' => [
          ':input[name="settings[label]"]' => ['filled' => TRUE],
        ],
      ],
    ];
    /** @var \Drupal\search_api_decoupled\SearchApiEndpointInterface $search_api_endpoint */
    $search_api_endpoint = $form_state->get('search_api_endpoint');
    $available_fields = $search_api_endpoint->getSortAllowedFields();
    foreach ($available_fields as $field => $label) {
      $form['info'][$field]['name'] = [
        '#markup' => $label,
      ];
      $form['info'][$field]['sortable'] = [
        '#title' => $this->t('Sortable for @field', ['@field' => $label]),
        '#title_display' => 'invisible',
        '#type' => 'checkbox',
        '#default_value' => !empty($this->configuration['info'][$field]['sortable']),
      ];
      $form['info'][$field]['asc'] = [
        '#type' => 'container',
      ];
      $form['info'][$field]['asc']['enabled'] = [
        '#title' => $this->t('Allow sortable by @field asc', ['@field' => $label]),
        '#title_display' => 'invisible',
        '#type' => 'checkbox',
        '#default_value' => !empty($this->configuration['info'][$field]['asc']['enabled']),
        '#states' => [
          'visible' => [
            ':input[name="settings[info][' . $field . '][sortable]"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['info'][$field]['asc']['label'] = [
        '#title' => $this->t('Label for sorting @field asc', ['@field' => $label]),
        '#type' => 'textfield',
        '#size' => 20,
        '#default_value' => !empty($this->configuration['info'][$field]['asc']['label']) ? $this->configuration['info'][$field]['asc']['label'] : $label,
        '#states' => [
          'visible' => [
            ':input[name="settings[info][' . $field . '][asc][enabled]"]' => ['checked' => TRUE],
          ],
          'required' => [
            ':input[name="settings[info][' . $field . '][asc][enabled]"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['info'][$field]['desc'] = [
        '#type' => 'container',
      ];
      $form['info'][$field]['desc']['enabled'] = [
        '#title' => $this->t('Allow sortable by @field desc', ['@field' => $label]),
        '#title_display' => 'invisible',
        '#type' => 'checkbox',
        '#default_value' => !empty($this->configuration['info'][$field]['desc']['enabled']),
        '#states' => [
          'visible' => [
            ':input[name="settings[info][' . $field . '][sortable]"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['info'][$field]['desc']['label'] = [
        '#title' => $this->t('Label for sorting @field desc', ['@field' => $label]),
        '#type' => 'textfield',
        '#size' => 20,
        '#default_value' => !empty($this->configuration['info'][$field]['desc']['label']) ? $this->configuration['info'][$field]['desc']['label'] : $label,
        '#states' => [
          'visible' => [
            ':input[name="settings[info][' . $field . '][desc][enabled]"]' => ['checked' => TRUE],
          ],
          'required' => [
            ':input[name="settings[info][' . $field . '][desc][enabled]"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['info'][$field]['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight'),
        '#title_display' => 'invisible',
        '#default_value' => $this->configuration['info'][$field]['weight'] ?? 0,
        '#attributes' => [
          'class' => [
            'draggable-weight',
          ],
        ],
      ];
    }
    // Special row for sorting by relevance.
    $form['info']['search_api_relevance']['name'] = ['#markup' => $this->t('None (Sort by relevance)')];
    $form['info']['search_api_relevance']['sortable'] = [
      '#title' => $this->t('Sortable for @field', ['@field' => $this->t('Relevance')]),
      '#title_display' => 'invisible',
      '#type' => 'checkbox',
      '#default_value' => !empty($this->configuration['info']['search_api_relevance']['sortable']),
    ];
    $form['info']['search_api_relevance']['asc'] = [
      '#type' => 'container',
    ];
    $form['info']['search_api_relevance']['asc']['enabled'] = [
      '#type' => 'value',
      '#value' => FALSE,
    ];
    $form['info']['search_api_relevance']['asc']['label'] = [
      '#type' => 'value',
      '#value' => '',
    ];
    $form['info']['search_api_relevance']['desc'] = [
      '#type' => 'container',
    ];
    $form['info']['search_api_relevance']['desc']['enabled'] = [
      '#title' => $this->t('Allow sortable by @field desc', ['@field' => $this->t('Relevance')]),
      '#title_display' => 'invisible',
      '#type' => 'checkbox',
      '#default_value' => !empty($this->configuration['info']['search_api_relevance']['desc']['enabled']),
      '#states' => [
        'visible' => [
          ':input[name="settings[info][search_api_relevance][sortable]"]' => ['checked' => TRUE],
        ],
        'checked' => [
          ':input[name="settings[info][search_api_relevance][sortable]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['info']['search_api_relevance']['desc']['label'] = [
      '#title' => $this->t('Label for sorting @field desc', ['@field' => $this->t('Relevance')]),
      '#type' => 'textfield',
      '#size' => 20,
      '#default_value' => !empty($this->configuration['info']['search_api_relevance']['desc']['label']) ? $this->configuration['info']['search_api_relevance']['desc']['label'] : $this->t('Relevance'),
      '#states' => [
        'visible' => [
          ':input[name="settings[info][search_api_relevance][desc][enabled]"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="settings[info][search_api_relevance][desc][enabled]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['info']['search_api_relevance']['weight'] = [
      '#type' => 'weight',
      '#title' => $this->t('Weight'),
      '#title_display' => 'invisible',
      '#default_value' => $this->configuration['info']['search_api_relevance']['weight'] ?? 0,
      '#attributes' => [
        'class' => [
          'draggable-weight',
        ],
      ],
    ];
    $form['description_markup'] = [
      '#type' => 'container',
    ];
    $form['description_markup'][] = [
      '#markup' => '<p>' . $this->t('Fields enabled for sorting will be displayed in the table below. You can drag and drop the rows to change the order of the fields.') . '</p>',
    ];
    $form['description_markup'][] = [
      '#markup' => '<p>' . $this->t('Default sorting is set to: "@field_name" (@order)', [
        '@field_name' => $available_fields[$search_api_endpoint->getDefaultSort()] ?? 'Relevance',
        '@order' => $search_api_endpoint->getDefaultSortOrder() == 'asc' ? $this->t('Ascending') : $this->t('Descending'),
      ]) . '</p>',
    ];
    $form['#theme'] = 'search_api_decoupled_ui_sorting_table';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $info = $form_state->getValue('info');
    // Set empty label for disabled sorting.
    foreach ($info as $field => $field_config) {
      foreach (['asc', 'desc'] as $direction) {
        if (empty($info[$field][$direction]['enabled'])) {
          $info[$field][$direction]['label'] = '';
        }
        // Cast to boolean.
        $info[$field][$direction]['enabled'] = (bool) $info[$field][$direction]['enabled'];
      }
      // Cast to boolean.
      $info[$field]['sortable'] = (bool) $info[$field]['sortable'];
    }
    $this->configuration['info'] = $info;
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $field_names = [];
    foreach ($this->configuration['info'] as $field => $field_config) {
      if (!empty($field_config['sortable'])) {
        $field_names[] = $field;
      }
    }
    return [
      '#markup' => $this->t('Fields enabled for sorting: @field_names.', ['@field_names' => implode(', ', $field_names)]),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSearchUiConfig() {
    $config = parent::getSearchUiConfig();
    if (!empty($config['settings']['info'])) {
      // Show only enabled for sorting fields.
      $config['settings']['info'] = array_filter($config['settings']['info'], function ($field_config) {
        return !empty($field_config['sortable']);
      });
      foreach ($config['settings']['info'] as $field => $field_config) {
        foreach (['asc', 'desc'] as $direction) {
          if (empty($config['settings']['info'][$field][$direction]['enabled'])) {
            unset($config['settings']['info'][$field][$direction]['label']);
          }
        }
      }
    }
    return $config;
  }

}
