<?php

namespace Drupal\search_api_decoupled_ui\Plugin\search_ui_element;

use Drupal\search_api_decoupled_ui\ConfigurableSearchUiElementBase;

/**
 * Plugin for Search UI Element "Load More".
 *
 * @SearchUiElement(
 *   id = "loadmore",
 *   label = @Translation("Load More (pager)"),
 * )
 */
class LoadMore extends ConfigurableSearchUiElementBase {

}
