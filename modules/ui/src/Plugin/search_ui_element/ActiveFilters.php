<?php

namespace Drupal\search_api_decoupled_ui\Plugin\search_ui_element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api_decoupled_ui\ConfigurableSearchUiElementBase;

/**
 * Provides a search UI element for active filters.
 *
 * @SearchUiElement(
 *   id = "active_filters",
 *   label = @Translation("Active filters"),
 *   description = @Translation("Displays the active filters for the search."),
 * )
 */
class ActiveFilters extends ConfigurableSearchUiElementBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'show_reset' => TRUE,
      'reset_text' => 'Clear all',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return [
      '#markup' => $this->t('Reset button label: @label.', ['@label' => $this->configuration['reset_text']]),
    ];
  }

}
