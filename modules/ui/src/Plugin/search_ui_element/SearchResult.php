<?php

namespace Drupal\search_api_decoupled_ui\Plugin\search_ui_element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api_decoupled_ui\ConfigurableSearchUiElementBase;

/**
 * Provide configuration for search result.
 *
 * @SearchUiElement(
 *   id = "search_result",
 *   label = @Translation("Rendered search result"),
 * )
 */
class SearchResult extends ConfigurableSearchUiElementBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'field' => '',
      'no_results' => $this->t('No results found.'),
      'loading' => $this->t('Loading...'),
      'columns' => 1,
      'show_loading' => TRUE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $search_api_endpoint = $form_state->get('search_api_endpoint');
    $available_fields = $search_api_endpoint->getAvailableFields();
    $options = ['' => $this->t('- Select -')];
    foreach ($available_fields as $field_name => $field_label) {
      $options[$field_name] = $field_label;
    }
    $form['field'] = [
      '#type' => 'select',
      '#title' => $this->t('Field with rendered search result'),
      '#default_value' => $this->configuration['field'],
      '#required' => TRUE,
      '#options' => $options,
      '#weight' => 1,
    ];
    $form['columns'] = [
      '#type' => 'number',
      '#title' => $this->t('Columns'),
      '#default_value' => $this->configuration['columns'],
      '#min' => 1,
      '#max' => 4,
      '#weight' => 3,
    ];
    $form['no_results'] = [
      '#type' => 'textfield',
      '#title' => $this->t('No results message'),
      '#default_value' => $this->configuration['no_results'],
      '#weight' => 4,
    ];
    $form['show_loading']['#weight'] = 5;
    $form['loading'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Loading message'),
      '#default_value' => $this->configuration['loading'],
      '#weight' => 6,
    ];
    $form['show_reset']['#access'] = FALSE;
    $form['reset_text']['#access'] = FALSE;
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['field'] = $form_state->getValue('field');
    $this->configuration['no_results'] = $form_state->getValue('no_results');
    $this->configuration['loading'] = $form_state->getValue('loading');
    $this->configuration['columns'] = $form_state->getValue('columns');
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return [
      '#markup' => $this->t('Field with HTML: @field in @columns column(s).', [
        '@field' => $this->configuration['field'],
        '@columns' => $this->configuration['columns'],
      ]),
    ];
  }

}
