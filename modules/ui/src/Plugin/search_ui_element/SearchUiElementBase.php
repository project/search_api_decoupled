<?php

namespace Drupal\search_api_decoupled_ui\Plugin\search_ui_element;

use Drupal\Core\Entity\DependencyTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\search_api_decoupled_ui\SearchUiElementInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a base class for search ui elements.
 */
class SearchUiElementBase extends PluginBase implements SearchUiElementInterface, ContainerFactoryPluginInterface {

  use DependencyTrait;

  /**
   * The search ui element ID.
   *
   * @var string
   */
  protected $uuid;

  /**
   * The weight of the search ui element.
   *
   * @var int|string
   */
  protected $weight = '';

  /**
   * The region of the search ui element.
   *
   * @var string
   */
  protected $region = '';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->setConfiguration($configuration);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getUuid() {
    return $this->uuid;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    $this->weight = $weight;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->weight;
  }

  /**
   * {@inheritdoc}
   */
  public function getRegion() {
    return $this->region;
  }

  /**
   * {@inheritdoc}
   */
  public function setRegion($region) {
    $this->region = $region;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return [
      'uuid' => $this->getUuid(),
      'id' => $this->getPluginId(),
      'weight' => $this->getWeight(),
      'region' => $this->getRegion(),
      'settings' => $this->configuration,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $configuration += [
      'settings' => [],
      'uuid' => '',
      'weight' => '',
      'region' => '',
    ];
    $this->configuration = $configuration['settings'] + $this->defaultConfiguration();
    $this->uuid = $configuration['uuid'];
    $this->weight = $configuration['weight'];
    $this->region = $configuration['region'];
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getSearchUiConfig() {
    $result = [
      'id' => $this->getUuid(),
      'type' => $this->getPluginId(),
      'weight' => $this->getWeight(),
    ];
    $configuration = $this->getConfiguration();
    $settings = $configuration['settings'] ?? [];
    if (!empty($settings['behaviors'])) {
      $behaviors = $settings['behaviors'];
      $result['behaviors'] = $behaviors;
      unset($settings['behaviors']);
    }
    if (!empty($settings['widget'])) {
      $widget = $settings['widget'];
      $result['widget'] = $widget;
      unset($settings['widget']);
    }
    // Do not include empty custom_element setting.
    if (isset($settings['custom_element']) && empty($settings['custom_element'])) {
      unset($settings['custom_element']);
    }
    if (!empty($settings)) {
      $result['settings'] = $settings;
    }
    return $result;
  }

}
