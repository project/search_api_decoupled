<?php

namespace Drupal\search_api_decoupled_ui\Plugin\search_ui_element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api_decoupled_ui\ConfigurableSearchUiElementBase;

/**
 * Provide configuration form for search summary.
 *
 * @SearchUiElement(
 *   id = "summary",
 *   label = @Translation("Search summary"),
 * )
 */
class Summary extends ConfigurableSearchUiElementBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['show_reset']['#access'] = FALSE;
    $form['reset_text']['#access'] = FALSE;
    return $form;
  }

}
