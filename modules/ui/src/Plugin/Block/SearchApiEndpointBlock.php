<?php

namespace Drupal\search_api_decoupled_ui\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Layout\LayoutPluginManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block with decoupled search.
 *
 * @Block(
 *   id = "search_api_endpoint",
 *   admin_label = @Translation("Search API Endpoint"),
 *   category = @Translation("Search API Decoupled"),
 *   deriver = "Drupal\search_api_decoupled_ui\Plugin\Derivative\SearchApiEndpointBlock",
 * )
 */
class SearchApiEndpointBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The layout plugin manager.
   *
   * @var \Drupal\Core\Layout\LayoutPluginManagerInterface
   */
  protected $layoutPluginManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new SearchBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Layout\LayoutPluginManagerInterface $layout_plugin_manager
   *   The layout plugin manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, LayoutPluginManagerInterface $layout_plugin_manager, ModuleHandlerInterface $module_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->layoutPluginManager = $layout_plugin_manager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.core.layout'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    /** @var \Drupal\search_api_decoupled_ui\SearchApiEndpointUiInterface $search_api_endpoint */
    $search_api_endpoint = $this->entityTypeManager->getStorage('search_api_endpoint')->load($this->getDerivativeId());
    $content = [];
    $layout = $this->layoutPluginManager->getDefinition($search_api_endpoint->getLayout());
    $layout_regions = $layout->getRegions();
    foreach (array_keys($search_api_endpoint->getUiElementRegions()) as $region) {
      $content[$region] = ['#markup' => '<p class="visually-hidden">' . $this->t('Region @region', ['@region' => $layout_regions[$region]['label']]) . '</p>'];
    }
    $cacheable_metadata = new CacheableMetadata();
    $block_id = 'block-' . $this->getMachineNameSuggestion();
    $ui_settings = $search_api_endpoint->getUiSettings($cacheable_metadata);

    $attached_libraries = array_merge([
      'search_api_decoupled_ui/client',
      $layout->getLibrary(),
    ], $this->getUiLibrariesToAttach($ui_settings));

    $build = $content + [
      '#theme' => $layout->getThemeHook(),
      '#layout' => $layout,
      '#attributes' => [
        'data-block-id' => $block_id,
      ],
      '#attached' => [
        'library' => $attached_libraries,
        'drupalSettings' => [
          'search_api_endpoint' => [
            $block_id => $ui_settings,
          ],
        ],
      ],
    ];

    // Allow other modules to alter the block.
    $context = [
      'cacheable_metadata' => $cacheable_metadata,
      'search_api_endpoint' => $search_api_endpoint,
      'block' => $this,
    ];

    $this->moduleHandler->alter('sae_ui_block', $build, $context);
    $cacheable_metadata->applyTo($build);

    return $build;
  }

  /**
   * Collect ui libraries that should be attached.
   *
   * Each UI element can attach its own library. This method checks if element
   * is used in current endpoint and if it is, it adds its library to the list.
   *
   * @param array $ui_settings
   *   UI settings for current endpoint.
   *
   * @return array
   *   The list of libraries to attach.
   */
  protected function getUiLibrariesToAttach(array $ui_settings) {
    $libraries = [];
    foreach ($ui_settings["elements"] as $element) {
      $library_to_add = '';
      switch ($element['type']) {
        case 'search_input':
          $library_to_add = 'search_api_decoupled_ui/ui-component--input';
          break;

        case 'facet':
          $library_to_add = 'search_api_decoupled_ui/ui-component--chip';
          break;

        case 'summary':
          $library_to_add = 'search_api_decoupled_ui/ui-component--summary';
          break;

        case 'search_result_custom':
          $library_to_add = 'search_api_decoupled_ui/ui-component--result';
          break;

        case 'pager':
          $library_to_add = 'search_api_decoupled_ui/ui-component--pager';
          break;
      }
      if (!empty($library_to_add) && !in_array($library_to_add, $libraries)) {
        $libraries[] = $library_to_add;
      }
    }
    $libraries[] = 'search_api_decoupled_ui/ui-component--wrapper';
    return $libraries;
  }

}
