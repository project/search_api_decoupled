<?php

namespace Drupal\search_api_decoupled_ui\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\search_api_decoupled_ui\Form\SearchBlockForm;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormBuilderInterface;

/**
 * Provides a block with non-decoupled search.
 *
 * @Block(
 *   id = "search_api_decoupled_ui_search_block",
 *   admin_label = @Translation("Search API Search Block with redirect"),
 *   category = @Translation("Search API Decoupled"),
 *   deriver = "Drupal\search_api_decoupled_ui\Plugin\Derivative\SearchApiEndpointBlock",
 * )
 */
class SearchBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The form builder service.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * SearchBlock constructor.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $form_builder, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $form_builder;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'redirect_path' => '',
      'placeholder' => '',
      'autocomplete' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['redirect_path'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Redirect path'),
      '#default_value' => $this->configuration['redirect_path'] ?? NULL,
    ];
    $form['placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Placeholder'),
      '#default_value' => $this->configuration['placeholder'] ?? NULL,
    ];
    /** @var \Drupal\search_api_decoupled\SearchApiEndpointInterface $search_api_endpoint */
    $search_api_endpoint = $this->entityTypeManager->getStorage('search_api_endpoint')->load($this->getDerivativeId());
    $autocomplete = $search_api_endpoint->getAutocomplete();
    if ($autocomplete) {
      $form['autocomplete'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Use Autocomplete'),
        '#default_value' => $this->configuration['autocomplete'] ?? FALSE,
      ];
    }
    else {
      $form['autocomplete'] = [
        '#value' => 'checkbox',
        '#default_value' => FALSE,
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    $uri = $form_state->getValue('redirect_path');
    if (!in_array($uri[0], ['/', '?', '#'], TRUE) && substr($uri, 0, 7) !== '<front>') {
      $form_state->setError($form['redirect_path'], new TranslatableMarkup('Manually entered paths should start with one of the following characters: / ? #'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['redirect_path'] = $form_state->getValue('redirect_path');
    $this->configuration['placeholder'] = $form_state->getValue('placeholder');
    $this->configuration['autocomplete'] = $form_state->getValue('autocomplete');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return $this->formBuilder->getForm(SearchBlockForm::class, $this->getDerivativeId(), $this->configuration);
  }

}
