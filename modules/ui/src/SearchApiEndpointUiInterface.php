<?php

namespace Drupal\search_api_decoupled_ui;

use Drupal\Core\Cache\CacheableMetadata;

/**
 * Defines an interface for search api endpoint ui elements.
 */
interface SearchApiEndpointUiInterface {

  /**
   * Retrieves a list of all available fields.
   *
   * @return string[]
   *   An options list of field identifiers mapped to their prefixed labels.
   */
  public function getAvailableFields();

  /**
   * The layout for search ui elements.
   *
   * @return string
   *   Can be any ID of a layout.
   */
  public function getLayout();

  /**
   * Gets the list of regions for the search ui elements.
   *
   * @return array
   *   The list of regions keyed by region name and label as value.
   */
  public function getUiElementRegions();

  /**
   * Gets the default region for the search ui elements.
   *
   * @return string
   *   The default region.
   */
  public function getDefaultUiElementRegion();

  /**
   * Gets the element from collection.
   *
   * @param string $element
   *   The element ID.
   *
   * @return \Drupal\search_api_decoupled_ui\SearchUiElementInterface
   *   The UI element.
   */
  public function getElement($element);

  /**
   * Gets the elements collection.
   *
   * @return \Drupal\search_api_decoupled_ui\SearchUiElementsPluginCollection
   *   The list of UI elements for this endpoint keyed by element ID.
   */
  public function getElements();

  /**
   * Gets the plugin collections.
   *
   * @return array
   *   The list of plugin collections.
   */
  public function getPluginCollections();

  /**
   * Adds element to collection.
   *
   * @param array $configuration
   *   The element configuration.
   *
   * @return string
   *   The UUID of the element.
   */
  public function addElement(array $configuration);

  /**
   * Deletes element from collection.
   *
   * @param \Drupal\search_api_decoupled_ui\SearchUiElementInterface $element
   *   The element to delete.
   *
   * @return \Drupal\search_api_decoupled\SearchApiEndpointInterface
   *   The endpoint.
   */
  public function deleteElement(SearchUiElementInterface $element);

  /**
   * Gets the UI settings.
   *
   * @param \Drupal\Core\Cache\CacheableMetadata|null $cacheable_metadata
   *   The cacheable metadata.
   * @param bool $absolute_url
   *   Whether to return absolute URLs.
   *
   * @return array
   *   The UI settings.
   */
  public function getUiSettings(CacheableMetadata $cacheable_metadata = NULL, $absolute_url = FALSE);

  /**
   * Gets the state manager.
   *
   * @return string
   *   The state manager.
   */
  public function getStateManager();

  /**
   * Sets the state manager.
   *
   * @param string $state_manager
   *   The state manager.
   */
  public function setStateManager($state_manager);

  /**
   * Checks whether the dedicated search page is needed.
   *
   * @return bool
   *   Whether provide a dedicated search page or not.
   */
  public function withSearchPage();

}
