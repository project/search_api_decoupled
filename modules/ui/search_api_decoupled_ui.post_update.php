<?php

/**
 * @file
 * Post update hooks for search_api_decoupled_ui module.
 */

use Drupal\Core\Config\Entity\ConfigEntityUpdater;
use Drupal\search_api_decoupled_ui\SearchApiEndpointUiInterface;

/**
 * Set state_manager to "url" for all search_api_endpoint entities.
 */
function search_api_decoupled_ui_post_update_10001(&$sandbox) {
  $config_entity_updater = \Drupal::classResolver(ConfigEntityUpdater::class);
  $callback = function (SearchApiEndpointUiInterface $search_api_endpoint) {
    $search_api_endpoint->setStateManager('url');
    return TRUE;
  };
  $config_entity_updater->update($sandbox, 'search_api_endpoint', $callback);
}

/**
 * Update endpoints to reflect new element's settings.
 */
function search_api_decoupled_ui_post_update_10002(&$sandbox) {
  $config_entity_updater = \Drupal::classResolver(ConfigEntityUpdater::class);
  $callback = function (SearchApiEndpointUiInterface $search_api_endpoint) {
    $elements = $search_api_endpoint->get('elements');
    foreach ($elements as $key => $element) {
      if (!empty($element['settings'])) {
        $elements[$key]['settings'] = $element['settings'] + [
          'show_label' => FALSE,
          'show_reset' => FALSE,
          'label' => '',
          'reset_text' => '',
          'orientation' => 'horizontal',
          'custom_element' => '',
        ];
        foreach ($element['settings'] as $setting_key => $setting) {
          switch ($setting_key) {
            case 'show_title':
              $elements[$key]['settings']['show_label'] = $setting;
              unset($elements[$key]['settings']['show_title']);
              break;

            case 'show_reset_link':
              $elements[$key]['settings']['show_reset'] = $setting;
              unset($elements[$key]['settings']['show_reset_link']);
              break;

            case 'title':
              $elements[$key]['settings']['label'] = $setting;
              unset($elements[$key]['settings']['title']);
              break;
          }
        }
      }
    }
    $search_api_endpoint->set('elements', $elements);
    return TRUE;
  };
  $config_entity_updater->update($sandbox, 'search_api_endpoint', $callback);
}
